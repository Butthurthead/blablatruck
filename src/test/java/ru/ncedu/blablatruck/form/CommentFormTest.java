package ru.ncedu.blablatruck.form;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ncedu.blablatruck.domain.entities.Comment;
import ru.ncedu.blablatruck.domain.entities.User;
import ru.ncedu.blablatruck.form.forms.CommentForm;

import java.util.HashMap;

public class CommentFormTest extends BaseFormTest {
    @Before
    public void init() {
        formClass = CommentForm.class;
    }

    @Test
    public void testCreationFromEntity() throws Exception {
        Comment comment = new Comment();
        comment.setText("bla bla");
        User user = new User();
        user.setUsername("User");
        comment.setUser(user);

        entity = comment;
        fromEntity();

        CommentForm commentForm = (CommentForm)form;
        Assert.assertEquals(comment.getUser().getUsername(), commentForm.getReceiver());
        Assert.assertEquals(comment.getText(), commentForm.getText());
    }

    @Test
    public void testCreationFromRequest() throws Exception {
        initReqData = new HashMap<>();
        initReqData.put("receiver", new String[] {"Reciever"});
        initReqData.put("text", new String[] {"Hello!"});

        fromReqMap();

        CommentForm commentForm = (CommentForm)form;
        Assert.assertEquals(initReqData.get("receiver")[0], commentForm.getReceiver());
        Assert.assertEquals(initReqData.get("text")[0], commentForm.getText());
    }
}
