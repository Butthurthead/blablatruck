package ru.ncedu.blablatruck.form;

import java.io.File;
import java.io.FileOutputStream;
import java.util.*;

import com.google.template.soy.data.SoyMapData;
import ru.ncedu.blablatruck.form.util.DarkMagicFormFactory;
import ru.ncedu.blablatruck.form.util.DefaultFormDataExtractor;
import ru.ncedu.blablatruck.form.util.FormDataExtractor;
import ru.ncedu.blablatruck.templateutil.PageRenderer;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

public class BaseFormTest {
    protected Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    protected Object entity;
    protected Map<String, String[]> initReqData = new HashMap<>();
    protected Map<String, Object> initData = new HashMap<>();
    protected Class<?> formClass;
    Object form;
    Map<String, String> fieldsErrors;
    protected Map<String, Object> extractedData;
    String fileName = "test.html";

    protected <T> Map<String, String> collectErrors(Set<ConstraintViolation<T>> violations) {
        Map<String, String> errors = new HashMap<>();
        for (ConstraintViolation<T> violation : violations) {
            String key = violation.getPropertyPath().toString();
            if (!errors.containsKey(key)) {
                errors.put(key, violation.getMessage());
            } else {
                String value = errors.get(key);
                errors.put(key, value + ", " + violation.getMessage());
            }
        }
        return errors;
    }

    protected void fromMap() throws Exception {
        form = new DarkMagicFormFactory<>(formClass).createFromMap(initData);
    }

    protected void fromReqMap() throws Exception {
        form = new RequestFormFactory<>(formClass).createFromRequestParametersMap(initReqData);
    }

    protected void fromEntity() throws Exception {
        form = new DarkMagicFormFactory<>(formClass).createFromEntity(entity);
    }

    protected void extractData() throws Exception {
        FormDataExtractor extractor = new DefaultFormDataExtractor<>(formClass);
        extractedData = extractor.extract(form, validator.validate(form));

        extractedData.put("csrfParameterName", "_csrf");
        extractedData.put("csrfToken", "hshhshf@&hf38f9");

        System.out.println(extractedData);
    }

    protected void renderForm() throws Exception {
        SoyMapData templateData = PageRenderer.createMainMenuData("Form test", "formTest", null);
        templateData.put("formData", extractedData);
        FileOutputStream fos = new FileOutputStream(new File(fileName));
        fos.write(("<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\" />\n" +
                "</head>\n" +
                "<body>" + PageRenderer.render(PageRenderer.DEFAULT_TEMPLATE, templateData) + "</body>" +
                "</html>").getBytes());
        fos.close();
    }

}
