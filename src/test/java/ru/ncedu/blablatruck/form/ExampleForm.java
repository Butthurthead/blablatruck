package ru.ncedu.blablatruck.form;

import ru.ncedu.blablatruck.form.annotation.Form;
import ru.ncedu.blablatruck.form.annotation.FormField;
import ru.ncedu.blablatruck.form.annotation.FormFieldOption;
import ru.ncedu.blablatruck.form.converters.RequestParameterValueConverter;

import java.util.*;

@Form(action = "/reg", failMessage = "Введите корректные данные")
public class ExampleForm {

    @FormFieldOption
    static class Age {
        private String label;
        private Integer value;
        public Age(String label, Integer value) {
            this.label = label;
            this.value = value;
        }
        public Integer getValue() {return value;}
        public String getLabel() {return label;}
    }

    @FormFieldOption
    static class Colour {
        private String label;
        private String value;
        public Colour(String label, String value) {
            this.label = label;
            this.value = value;
        }
        public String getValue() {return value;}
        public String getLabel() {return label;}
    }

    public static List<Age> ageOptions;

    public static List<Colour> coloursOptions;

    public static List<Age> numbersOptions;

    static {
        ageOptions = new ArrayList<>();
        ageOptions.add(new Age("18 years", 18));
        ageOptions.add(new Age("35 years", 35));
        ageOptions.add(new Age("50 years", 50));

        coloursOptions = new ArrayList<>();
        coloursOptions.add(new Colour("Синий", "blue"));
        coloursOptions.add(new Colour("Красный", "red"));
        coloursOptions.add(new Colour("Зелёный", "green"));

        numbersOptions = new ArrayList<>();
        numbersOptions.add(new Age("1", 1));
        numbersOptions.add(new Age("10", 10));
        numbersOptions.add(new Age("100", 100));
        numbersOptions.add(new Age("1000", 1000));
        numbersOptions.add(new Age("10000", 10000));
        numbersOptions.add(new Age("100000", 100000));
    }

    @FormField(formFieldName = "username", entityFieldName = "username", formFieldLabel = "имя пользователя",
            valueConverter = RequestParameterValueConverter.class)
    private String username;

    @FormField(formFieldName = "email", entityFieldName = "email", formFieldLabel = "email",
            valueConverter = RequestParameterValueConverter.class)
    private String email;

    @FormField(formFieldName = "phone", entityFieldName = "phone", formFieldLabel = "телефон",
            valueConverter = RequestParameterValueConverter.class)
    private String phone;

    @FormField(formFieldName = "age", formFieldOptions = "ageOptions",
            formFieldLabel = "возраст", formFieldType = "select",
            valueConverter = RequestParameterValueConverter.class)
    private Integer age;

    @FormField(formFieldName = "password", entityFieldName = "password", formFieldLabel = "пароль",
            valueConverter = RequestParameterValueConverter.class)
    private String password;

    @FormField(formFieldName = "passwordRepeat", entityFieldName = "password", formFieldLabel = "повторите пароль",
            valueConverter = RequestParameterValueConverter.class)
    private String passwordRepeat;

    @FormField(formFieldName = "colours", formFieldLabel = "любимые цвета",
            formFieldType = "checkbox", formFieldOptions = "coloursOptions",
            valueConverter = RequestParameterValueConverter.class)
    private List<String> colours;

    @FormField(formFieldName = "numbers", formFieldLabel = "любимые степени числа 10",
            formFieldType = "checkbox", formFieldOptions = "numbersOptions",
            valueConverter = RequestParameterValueConverter.class)
    private List<Integer> numbers;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }

    public List<String> getColours() { return colours; }

    public void setColours(List<String> colours) { this.colours = colours; }

    public List<Integer> getNumbers() { return numbers; }

    public void setNumbers(List<Integer> numbers) { this.numbers = numbers; }
}
