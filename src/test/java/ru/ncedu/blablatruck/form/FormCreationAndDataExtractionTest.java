package ru.ncedu.blablatruck.form;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ncedu.blablatruck.domain.entities.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static ru.ncedu.blablatruck.templateutil.TemplateTags.FormTags.*;

public class FormCreationAndDataExtractionTest extends BaseFormTest {

    protected Map<String, Object> extractedField(String fieldName) {
        List<Map<String, Object>> fields = (List<Map<String, Object>>) extractedData.get("fields");
        for (Map<String, Object> field : fields) {
            if (field.get(NAME).equals(fieldName)) {
                return field;
            }
        }
        return null;
    }

    protected List<Map<String, Object>> extractedFieldOptions(String fieldName) {
        List<Map<String, Object>> fields = (List<Map<String, Object>>) extractedData.get("fields");
        for (Map<String, Object> field : fields) {
            if (field.get(NAME).equals(fieldName)) {
                return (List<Map<String, Object>>)field.get(OPTIONS);
            }
        }
        return null;
    }

    protected void checkCreation(Function<String, Object> getInit) throws Exception {
        ExampleForm exForm = (ExampleForm)form;
        Assert.assertEquals(getInit.apply("username"), exForm.getUsername());
        Assert.assertEquals(getInit.apply("email"), exForm.getEmail());
        Assert.assertEquals(getInit.apply("phone"), exForm.getPhone());
        Assert.assertEquals(getInit.apply("age"), "" + exForm.getAge());
        Assert.assertEquals(getInit.apply("password"), exForm.getPassword());
        Assert.assertEquals(getInit.apply("passwordRepeat"), exForm.getPasswordRepeat());
        String[] initColours = (String[])getInit.apply("colours");
        if (initColours != null) {
            int i = 0;
            for (String s : initColours) {
                Assert.assertEquals(s, exForm.getColours().get(i));
                ++i;
            }
        }
        String[] initNumbers = (String[])getInit.apply("numbers");
        if (initNumbers != null) {
            int i = 0;
            for (String s : initNumbers) {
                Assert.assertEquals(s, exForm.getNumbers().get(i).toString());
                ++i;
            }
        }
    }

    public void checkCreationFromReqData() throws Exception {
        checkCreation(key -> {
            if (key.equals("colours") || key.equals("numbers")) {
                return initReqData.get(key);
            }
            return initReqData.get(key)[0];
        });
    }

    protected void checkExtraction() {
        ExampleForm exForm = (ExampleForm)form;
        Assert.assertEquals(exForm.getUsername(), extractedField("username").get(VALUE));
        Assert.assertEquals(exForm.getEmail(), extractedField("email").get(VALUE));
        Assert.assertEquals(exForm.getPhone(), extractedField("phone").get(VALUE));
        Assert.assertEquals(exForm.getPassword(), extractedField("password").get(VALUE));
        Assert.assertEquals(exForm.getPasswordRepeat(), extractedField("passwordRepeat").get(VALUE));

        int i = 0;
        for (ExampleForm.Age ageOpt : ExampleForm.ageOptions) {
            Assert.assertEquals(ageOpt.getValue(), extractedFieldOptions("age").get(i).get(OPTION_VALUE));
            Assert.assertEquals(ageOpt.getLabel(), extractedFieldOptions("age").get(i).get(OPTION_LABEL));
            ++i;
        }
        i = 0;
        for (ExampleForm.Colour clrOpt : ExampleForm.coloursOptions) {
            Assert.assertEquals(clrOpt.getValue(), extractedFieldOptions("colours").get(i).get(OPTION_VALUE));
            Assert.assertEquals(clrOpt.getLabel(), extractedFieldOptions("colours").get(i).get(OPTION_LABEL));
            ++i;
        }
        i = 0;
        for (ExampleForm.Age numOpt : ExampleForm.numbersOptions) {
            Assert.assertEquals(numOpt.getValue(), extractedFieldOptions("numbers").get(i).get(OPTION_VALUE));
            Assert.assertEquals(numOpt.getLabel(), extractedFieldOptions("numbers").get(i).get(OPTION_LABEL));
            ++i;
        }

        Assert.assertNull(extractedField("colours").get(VALUE));
        Assert.assertNotNull(extractedField("colours").get(VALUES));
    }

    protected void initReqParameterMap() {
        initReqData = new HashMap<>();
        initReqData.put("username", new String[] {"vasya"});
        initReqData.put("email", new String[] {"vasya@vasya.com"});
        initReqData.put("phone", new String[] {"88005553535"});
        initReqData.put("password", new String[] {"qwerty"});
        initReqData.put("passwordRepeat", new String[] {"qwerty1"});
        initReqData.put("age", new String[] {"18"});
        initReqData.put("colours", new String[] {"blue", "green"});
        initReqData.put("numbers", new String[] {"1", "1000"});
    }

    protected void initEntity() {
        User user = new User();
        user.setUsername("doe");
        user.setEmail("doe@john.com");
        user.setPhone("55-44-33");
        user.setPassword("fdsfkdsjfklaf");
        user.setId(6610);
        entity = user;
    }

    @Before
    public void initFormClass() {
        formClass = ExampleForm.class;
    }

    @Test
    public void creatingFromMapAndExtractTest() throws Exception {
        initData = new HashMap<>();
        initData.put("username", "john");
        initData.put("email", "john@doe.com");
        initData.put("phone", "33-44-55");
        initData.put("password", "qwerty");
        initData.put("passwordRepeat", "qwerty1234");
        initData.put("age", "18");

        fileName = "creatingFromMapAndExtractTest.html";

        fromMap();

        ExampleForm exForm = (ExampleForm)form;
        List<String> colours = new ArrayList<>();
        colours.add("red");
        colours.add("green");
        exForm.setColours(colours);

        checkCreation(key -> initData.get(key));

        extractData();

        checkExtraction();
    }

    @Test
    public void createFromRequestAndExtractTest() throws Exception {
        initReqParameterMap();

        fromReqMap();
        checkCreationFromReqData();

        extractData();
        checkExtraction();
    }

    @Test
    public void creatingFromEntityAndRenderTest() throws Exception {
        initEntity();
        User user = (User)entity;

        fileName = "creatingFromEntityAndRenderTest.html";

        fromEntity();

        ExampleForm regForm = (ExampleForm)form;
        Assert.assertEquals(regForm.getUsername(), user.getUsername());
        Assert.assertEquals(regForm.getEmail(), user.getEmail());
        Assert.assertEquals(regForm.getPhone(), user.getPhone());
        Assert.assertEquals(regForm.getPassword(), user.getPassword());

        regForm.setAge(35);
        List<String> colours = new ArrayList<>();
        colours.add("red");
        colours.add("green");
        regForm.setColours(colours);
        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(100);
        regForm.setNumbers(numbers);

        extractData();

        renderForm();
    }

    @Test
    public void formInheritanceTest() throws Exception {
        formClass = InheritExampleForm.class;
        initReqParameterMap();
        initReqData.put("inheritField", new String[] {"inherit field value"});

        fromReqMap();
        checkCreationFromReqData();
        InheritExampleForm inhExForm = (InheritExampleForm)form;
        Assert.assertEquals("inherit field value", inhExForm.getInheritField());

        extractData();
        checkExtraction();
        Assert.assertEquals(inhExForm.getInheritField(), extractedField("inheritField").get(VALUE));
    }

}
