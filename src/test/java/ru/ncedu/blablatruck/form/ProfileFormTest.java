package ru.ncedu.blablatruck.form;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ncedu.blablatruck.form.forms.ProfileForm;

import javax.validation.ConstraintViolation;
import java.util.HashMap;
import java.util.Set;

public class ProfileFormTest extends BaseFormTest {
    @Before
    public void init() {
        initReqData = new HashMap<>();
        initReqData.put("username", new String[] {"user"});
        initReqData.put("phone", new String[] {"7777777"});
        initReqData.put("email", new String[] {"aaa@mail.ru"});
        initReqData.put("password", new String[] {"password"});
        initReqData.put("confirmPassword", new String[] {"password"});

        formClass = ProfileForm.class;
    }

    private void validateProfileForm() {
        Set<ConstraintViolation<ProfileForm>> violations = validator.validate((ProfileForm)form);
        fieldsErrors = collectErrors(violations);
    }

    @Test
    public void testPasswordMatching() throws Exception {
        initReqData.put("confirmPassword", null);
        fromReqMap();
        validateProfileForm();
        extractData();
        Assert.assertNotNull(fieldsErrors.get("confirmPassword"));
    }

    @Test
    public void testPasswordFormat() throws Exception {
        initReqData.put("password", new String[] {"a45b78dhdhd890"});
        fromReqMap();
        validateProfileForm();
        extractData();
        Assert.assertNull(fieldsErrors.get("password"));

        initReqData.put("password", null);
        fromReqMap();
        validateProfileForm();
        extractData();
        Assert.assertNull(fieldsErrors.get("password"));

        initReqData.put("password", new String[] {""});
        fromReqMap();
        validateProfileForm();
        extractData();
        Assert.assertNull(fieldsErrors.get("password"));

        initReqData.put("password", new String[] {"a45b"});
        fromReqMap();
        validateProfileForm();
        extractData();
        Assert.assertNotNull(fieldsErrors.get("password"));

        initReqData.put("password", new String[] {"    "});
        fromReqMap();
        validateProfileForm();
        extractData();
        Assert.assertNotNull(fieldsErrors.get("password"));

        initReqData.put("password", new String[] {"hhs&*S9sffs78f(()_)><<>>>"});
        fromReqMap();
        validateProfileForm();
        extractData();
        Assert.assertNotNull(fieldsErrors.get("password"));
    }

}
