package ru.ncedu.blablatruck.form;


import ru.ncedu.blablatruck.form.annotation.Form;
import ru.ncedu.blablatruck.form.annotation.FormField;

@Form(action = "")
public class InheritExampleForm extends ExampleForm {

    @FormField(formFieldName = "inheritField")
    private String inheritField;

    public String getInheritField() {
        return inheritField;
    }

    public void setInheritField(String inheritField) {
        this.inheritField = inheritField;
    }

}
