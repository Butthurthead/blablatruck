package ru.ncedu.blablatruck.form;

import ru.ncedu.blablatruck.form.util.DarkMagicFormFactory;

import java.util.Map;

/**
 * Inherits all the methods of DarkMagicFormFactory class besides the overriden createFromRequest().
 * @param <T>
 */
public class RequestFormFactory<T> extends DarkMagicFormFactory<T> {

    public RequestFormFactory(Class<T> clazz) {
        super(clazz);
    }

    public T createFromRequestParametersMap(Map<String, String[]> parameters)
            throws IllegalAccessException, InstantiationException, NoSuchFieldException {
        return createFrom(a -> parameters.get(a.formFieldName()));
    }
}
