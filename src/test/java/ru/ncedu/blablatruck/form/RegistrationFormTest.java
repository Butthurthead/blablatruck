package ru.ncedu.blablatruck.form;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import ru.ncedu.blablatruck.form.forms.RegistrationForm;
import ru.ncedu.blablatruck.util.ValidatorFactory;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.lang.annotation.Annotation;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsNot.not;

public class RegistrationFormTest {

    private Validator validator;
    private RegistrationForm registrationForm;

    private final static String PASSWORD_ONE = "qwerty";
    private final static String PASSWORD_TWO = "QWERTY";

    @Before
    public void setUp() throws Exception {
        ValidatorFactory factory = new ValidatorFactory();
        validator = factory.getObjectInstance(null, null, null, null);
        registrationForm = new RegistrationForm();
    }

    @Test
    public void testPasswordsMatchConstraintWithEqualsPasswords() throws Exception {
        registrationForm.setPassword(PASSWORD_ONE);
        registrationForm.setConfirmPassword(PASSWORD_ONE);
        Set<ConstraintViolation<RegistrationForm>> violations = validator.validate(registrationForm);
//        assertThat(violations, not(hasItem(is(RegistrationFormPasswordsMatch.class))));
    }

    @Test
    public void testPasswordsMatchConstraintWithNotEqualsPasswords() throws Exception {
        registrationForm.setPassword(PASSWORD_ONE);
        registrationForm.setConfirmPassword(PASSWORD_TWO);
        Set<ConstraintViolation<RegistrationForm>> violations = validator.validate(registrationForm);
//        assertThat(violations, hasItem(is(RegistrationFormPasswordsMatch.class)));
    }

    private <T> Matcher<ConstraintViolation<T>> is(Class<? extends Annotation> annotation) {
        return new TypeSafeMatcher<ConstraintViolation<T>>() {
            @Override
            protected boolean matchesSafely(ConstraintViolation<T> item) {
                return item.getConstraintDescriptor().getAnnotation().annotationType() == annotation;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(annotation.toString());
            }
        };
    }
}
