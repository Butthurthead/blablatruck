package ru.ncedu.blablatruck.entity.order;

import org.junit.Assert;
import org.junit.Test;
import ru.ncedu.blablatruck.domain.dao.OrderDAO;
import ru.ncedu.blablatruck.domain.dao.OrderStatus;
import ru.ncedu.blablatruck.domain.entities.Order;
import ru.ncedu.blablatruck.domain.entities.Trip;
import ru.ncedu.blablatruck.domain.entities.User;

import java.util.ArrayList;

public class OrderStatusUpdatingTest {
    private User owner, client;
    private Order order;

    public OrderStatusUpdatingTest() throws Exception {
        client = new User();
        client.setUsername("client");
        owner = new User();
        owner.setUsername("owner");
        Trip trip = new Trip();
        trip.setOwner(owner);
        trip.setOrders(new ArrayList<>());
        order = new Order();
        order.setTrip(trip);
        order.setClient(client);
        trip.getOrders().add(order);
    }

    private void check(User user, OrderStatus initStatus, OrderStatus newStatus, Class<? extends Throwable> exceptionClass) throws Throwable {
        boolean passed = false;
        try {
            order.setStatus(initStatus);
            OrderDAO.permitStatusUpdating(order, user.getUsername(), newStatus);
        } catch (Throwable t) {
            passed = t.getClass().equals(exceptionClass);
        }
        Assert.assertTrue(passed);
    }

    @Test
    public void run() throws Throwable {
        order.setStatus(OrderStatus.STATUS_NEW);
        OrderDAO.permitStatusUpdating(order, owner.getUsername(), OrderStatus.STATUS_COMMITTED);
        order.setStatus(OrderStatus.STATUS_COMMITTED);
        OrderDAO.permitStatusUpdating(order, client.getUsername(), OrderStatus.STATUS_SHIPPED);
        order.setStatus(OrderStatus.STATUS_SHIPPED);
        OrderDAO.permitStatusUpdating(order, client.getUsername(), OrderStatus.STATUS_DELIVERED);
        order.setStatus(OrderStatus.STATUS_COMMITTED);
        OrderDAO.permitStatusUpdating(order, client.getUsername(), OrderStatus.STATUS_REJECTED);
        order.setStatus(OrderStatus.STATUS_COMMITTED);
        OrderDAO.permitStatusUpdating(order, owner.getUsername(), OrderStatus.STATUS_COMMITTED);
        OrderDAO.permitStatusUpdating(order, owner.getUsername(), OrderStatus.STATUS_REJECTED);

        User user = new User();
        user.setUsername("user");
        check(user, OrderStatus.STATUS_NEW, OrderStatus.STATUS_COMMITTED, SecurityException.class);
        check(client, OrderStatus.STATUS_NEW, OrderStatus.STATUS_COMMITTED, UnsupportedOperationException.class);
        check(client, OrderStatus.STATUS_NEW, OrderStatus.STATUS_SHIPPED, UnsupportedOperationException.class);
        check(client, OrderStatus.STATUS_NEW, OrderStatus.STATUS_DELIVERED, UnsupportedOperationException.class);
        check(owner, OrderStatus.STATUS_COMMITTED, OrderStatus.STATUS_SHIPPED, UnsupportedOperationException.class);
        check(owner, OrderStatus.STATUS_SHIPPED, OrderStatus.STATUS_REJECTED, UnsupportedOperationException.class);
        check(owner, OrderStatus.STATUS_SHIPPED, OrderStatus.STATUS_DELIVERED, UnsupportedOperationException.class);
        check(client, OrderStatus.STATUS_SHIPPED, OrderStatus.STATUS_REJECTED, UnsupportedOperationException.class);
    }

}
