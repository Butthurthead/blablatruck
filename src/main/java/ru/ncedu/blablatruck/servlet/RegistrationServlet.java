package ru.ncedu.blablatruck.servlet;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.csrf.CsrfToken;
import ru.ncedu.blablatruck.domain.dao.UserDAO;
import ru.ncedu.blablatruck.domain.dao.implementations.HibernateUserDAO;
import ru.ncedu.blablatruck.form.forms.RegistrationForm;
import ru.ncedu.blablatruck.form.forms.groups.UniqueEmailGroup;
import ru.ncedu.blablatruck.form.util.DarkMagicFormFactory;
import ru.ncedu.blablatruck.form.util.DefaultFormDataExtractor;
import ru.ncedu.blablatruck.form.util.FormFactory;
import ru.ncedu.blablatruck.templateutil.HtmlResponseWriter;
import ru.ncedu.blablatruck.templateutil.TemplateTags;
import ru.ncedu.blablatruck.util.MailUtil;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import java.io.IOException;
import java.util.Calendar;
import java.util.Map;
import java.util.Set;

@WebServlet("/registration")
public class RegistrationServlet extends AppHttpServlet {

    private static final String CONTENT_DELEGATE = "registration";
    private static final String PAGE_TITLE = "Регистрация";
    private UserDAO userDAO = new HibernateUserDAO();

    @Resource(name = "java:comp/Validator")
    protected Validator validator;

    private FormFactory<RegistrationForm> registrationFormFactory;

    @Override
    public void init() throws ServletException {
        registrationFormFactory = new DarkMagicFormFactory<>(RegistrationForm.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        boolean isAnon = SecurityContextHolder.getContext()
                .getAuthentication() instanceof AnonymousAuthenticationToken;

        if (isAnon) {
            Map<String, Object> formData;
            try{
                RegistrationForm form = new DarkMagicFormFactory<>(RegistrationForm.class).createFromRequest(req);
                formData = new DefaultFormDataExtractor<>(RegistrationForm.class).extract(form, null);
            } catch (Exception e){
                e.printStackTrace(System.out);
                return;
            }
            HtmlResponseWriter.create(resp)
                    .setContentDelegate(CONTENT_DELEGATE)
                    .setTitle(PAGE_TITLE)
                    .setCsrfToken((CsrfToken) req.getAttribute("_csrf"))
                    .putData(TemplateTags.FORM_DATA, formData)
                    .write();
        } else {
            resp.sendRedirect("/profile");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        boolean isAnon = SecurityContextHolder.getContext()
                .getAuthentication() instanceof AnonymousAuthenticationToken;

        if (isAnon) {
            RegistrationForm form;
            try {
                form = registrationFormFactory.createFromRequest(req);
                Set<ConstraintViolation<RegistrationForm>> violations = validator.validate(form,
                        Default.class, UniqueEmailGroup.class);
                if (violations.size() == 0) {
                    userDAO.create(form);
                    //Send success mail-->
                    try {
                        //Get the mailer instance
                        MailUtil mailer = (MailUtil) mailCfg.getBean("mailService");
                        mailer.sendMail(form.getEmail(), form.getUsername(), TemplateTags.M_SUCCESS_REG);
                        //Send info for admin mail
                        mailer.sendPreConfiguredMail("Зарегистрировался пользователь: " +
                                form.getUsername() + ", " + form.getEmail() + " \n" +
                                "Дата регистрации: " +
                                Calendar.getInstance().getTime());
                    } catch (Exception e){
                        //TODO save error to log file
                    }
                    //<--
                    resp.sendRedirect("/login");
                } else {
                    Map<String, Object> formData = new DefaultFormDataExtractor<>(RegistrationForm.class).
                            extract(form, violations);
                    HtmlResponseWriter.create(resp)
                            .setContentDelegate(CONTENT_DELEGATE)
                            .setTitle(PAGE_TITLE)
                            .setCsrfToken((CsrfToken) req.getAttribute("_csrf"))
                            .putData(TemplateTags.FORM_DATA, formData)
                            .write();
                }
            } catch (Exception e) {
                throw new ServletException(e);
            }
        }
    }
}
