package ru.ncedu.blablatruck.servlet;

import org.springframework.security.web.csrf.CsrfToken;
import ru.ncedu.blablatruck.domain.dao.TripDAO;
import ru.ncedu.blablatruck.domain.dao.implementations.HibernateLocationDAO;
import ru.ncedu.blablatruck.domain.dao.implementations.HibernateTripDAO;
import ru.ncedu.blablatruck.domain.entities.Trip;
import ru.ncedu.blablatruck.form.forms.FindTripForm;
import ru.ncedu.blablatruck.form.util.DarkMagicFormFactory;
import ru.ncedu.blablatruck.form.util.DefaultFormDataExtractor;
import ru.ncedu.blablatruck.templateutil.HtmlResponseWriter;
import ru.ncedu.blablatruck.templateutil.TemplateTags;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.IOException;
import java.util.*;

@WebServlet("/findtrip")
public class FindTripServlet extends AppHttpServlet {
    private static final String FIND_TRIP_CONTENT_DELEGATE = "findTrip";
    private static final String FIND_TRIP_TITLE = "Поиск перевозок";
    private static final int TRIPS_ON_PAGE = 4;
    private TripDAO tripDAO = new HibernateTripDAO();

    @Resource(name = "java:comp/Validator")
    protected Validator validator;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            // AutoComplete start
            String action = req.getParameter(TemplateTags.ACTION);
            if (action != null && action.equals("complete")){
                String id = req.getParameter(TemplateTags.ID);
                List<String> founded = (new HibernateLocationDAO()).found(id);
                StringBuilder resuilt = new StringBuilder();
                if (founded.size() == 0){
                    resp.getWriter().print("E");
                    return;
                }
                resuilt.append(founded.get(0));
                for(int i = 1; i < founded.size(); i++){
                    resuilt.append(",").append(founded.get(i));
                }
                resp.getWriter().print(resuilt.toString());
                return;
            }
            // End AutoComplete
            FindTripForm form = new DarkMagicFormFactory<>(FindTripForm.class).createFromRequest(req);
            Set<ConstraintViolation<FindTripForm>> violations = validator.validate(form);

            String lastTripIdParam = req.getParameter(TemplateTags.LAST_TRIP_ID);

            if (lastTripIdParam != null){
                addMore(resp, form, Integer.valueOf(lastTripIdParam));
                return;
            }

            if (violations.size() == 0) {

                if (form.getTotalMass() == null) { form.setTotalMass(0.0); }
                if (form.getTotalV() == null) { form.setTotalV(0.0); }
                if (form.getArrivalCity() == null){ form.setArrivalCity(""); }
                if (form.getDepartureCity() == null){ form.setDepartureCity(""); }

                Map<String, Object> formData = new DefaultFormDataExtractor<>(FindTripForm.class).extract(form, null);
                List<Trip> trips = tripDAO.findTripList(form, -1, TRIPS_ON_PAGE);
                List<Map<String, Object>> templateData = tripsToTemplate(trips);
                int lastTripId = trips.size() == 0 ? 0 : trips.get(trips.size()-1).getId();
                boolean needMore = trips.size() == TRIPS_ON_PAGE;

                HtmlResponseWriter.create(resp)
                        .setContentDelegate(FIND_TRIP_CONTENT_DELEGATE)
                        .setTitle(FIND_TRIP_TITLE)
                        .setCsrfToken((CsrfToken) req.getAttribute("_csrf"))
                        .putData(TemplateTags.FORM_DATA, formData)
                        .putData(TemplateTags.TRIPS, templateData)
                        .putData(TemplateTags.LAST_TRIP_ID, lastTripId)
                        .putData(TemplateTags.NEED_MORE, needMore)
                        .write();
            } else {
                Map<String, Object> formData = new DefaultFormDataExtractor<>(FindTripForm.class).
                        extract(form, violations);
                HtmlResponseWriter.create(resp)
                        .setContentDelegate(FIND_TRIP_CONTENT_DELEGATE)
                        .setTitle(FIND_TRIP_TITLE)
                        .setCsrfToken((CsrfToken) req.getAttribute("_csrf"))
                        .putData(TemplateTags.FORM_DATA, formData)
                        .write();
            }
        } catch (Exception e){
            throw new ServletException(e);
        }
    }

    private List<Map<String, Object>> tripsToTemplate(List<Trip> trips) {
        if (trips != null) {
            List<Map<String, Object>> templateTrips = new ArrayList<>();
            for (Trip trip : trips) {
                Map<String, Object> templateTrip = new HashMap<>();
                templateTrip.put(TemplateTags.TRIP_ID, trip.getId());
                templateTrip.put(TemplateTags.TRIP_ARRIVAL, trip.getArrival());
                templateTrip.put(TemplateTags.TRIP_DEPARTURE, trip.getDeparture());
                templateTrip.put(TemplateTags.TRIP_DESCRIPTION, trip.getDescription());
                templateTrip.put(TemplateTags.TRIP_DIMX, trip.getDimX());
                templateTrip.put(TemplateTags.TRIP_DIMY, trip.getDimY());
                templateTrip.put(TemplateTags.TRIP_DIMZ, trip.getDimZ());
                templateTrip.put(TemplateTags.TRIP_PRICE_PER_KG, trip.getPricePerKg());
                templateTrip.put(TemplateTags.TRIP_PRICE_PER_M2, trip.getPricePerM2());
                templateTrip.put(TemplateTags.TRIP_STATUS, trip.getStatus().toString());
                templateTrip.put(TemplateTags.TRIP_TOTAL_MASS, trip.getTotalMass());
                templateTrip.put(TemplateTags.TRIP_DEPARTURE_CITY, trip.getDepartureCity());
                templateTrip.put(TemplateTags.TRIP_ARRIVAL_CITY, trip.getArrivalCity());

                templateTrips.add(templateTrip);
            }
            return templateTrips;
        }
        return new ArrayList<>();
    }

    private void addMore(HttpServletResponse resp, FindTripForm form, Integer lastTripId) throws IOException {
        List<Trip> trips = tripDAO.findTripList(form, lastTripId, TRIPS_ON_PAGE);
        int newLastTripId = trips.size() == 0 ? 0 : trips.get(trips.size()-1).getId();
        StringBuilder message = new StringBuilder();
        final String inTripSplitter = "->";
        final String tripSplitter = "<AND>";

        //'$trip.tripId->$trip.departure->$trip.departureCity->$trip.arrivalCity->$trip.description->$trip.status->$trip.arrival->$trip.totalMass->$trip.dimX->$trip.dimY->$trip.dimZ->$trip.pricePerKG->$trip.pricePerM2'

        for (int i = 0; i < trips.size(); i++) {
            message.append(trips.get(i).getId()).append(inTripSplitter)
                    .append(trips.get(i).getDeparture()).append(inTripSplitter)
                    .append(trips.get(i).getDepartureCity()).append(inTripSplitter)
                    .append(trips.get(i).getArrivalCity()).append(inTripSplitter)
                    .append(trips.get(i).getDescription()).append(inTripSplitter)
                    .append(trips.get(i).getStatus()).append(inTripSplitter)
                    .append(trips.get(i).getArrival()).append(inTripSplitter)
                    .append(trips.get(i).getTotalMass()).append(inTripSplitter)
                    .append(trips.get(i).getDimX()).append(inTripSplitter)
                    .append(trips.get(i).getDimY()).append(inTripSplitter)
                    .append(trips.get(i).getDimZ()).append(inTripSplitter)
                    .append(trips.get(i).getPricePerKg()).append(inTripSplitter)
                    .append(trips.get(i).getPricePerM2()).append(inTripSplitter);
            if (i != trips.size() - 1) {
                message.append(tripSplitter);
            }
        }
        message.append(TemplateTags.LAST_TRIP_ID).append(newLastTripId);
        resp.getWriter().print(message.toString());
        resp.getWriter().flush();
    }
}
