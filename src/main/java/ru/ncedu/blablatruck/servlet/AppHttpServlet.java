package ru.ncedu.blablatruck.servlet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.validation.Validator;
import java.io.PrintWriter;
import java.io.StringWriter;

public abstract class AppHttpServlet extends HttpServlet {

    @Resource(name = "java:comp/Validator")
    protected Validator validator;
    protected Log log = LogFactory.getLog(getClass());
    protected static ApplicationContext mailCfg = new ClassPathXmlApplicationContext("mail.cfg.xml");

    protected String getAuthUsername() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    protected void logStackTrace(Throwable throwable) {
        StringWriter stringWriter = new StringWriter();
        throwable.printStackTrace(new PrintWriter(stringWriter));
        String stackTrace = stringWriter.toString();
        log.error(stackTrace);
    }

}
