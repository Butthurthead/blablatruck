package ru.ncedu.blablatruck.servlet;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.csrf.CsrfToken;
import ru.ncedu.blablatruck.domain.dao.OrderDAO;
import ru.ncedu.blablatruck.domain.dao.OrderStatus;
import ru.ncedu.blablatruck.domain.dao.TripDAO;
import ru.ncedu.blablatruck.domain.dao.implementations.HibernateTripDAO;
import ru.ncedu.blablatruck.domain.entities.Order;
import ru.ncedu.blablatruck.domain.entities.Trip;
import ru.ncedu.blablatruck.domain.entities.TripStatus;
import ru.ncedu.blablatruck.domain.entities.User;
import ru.ncedu.blablatruck.templateutil.HtmlResponseWriter;
import ru.ncedu.blablatruck.templateutil.TemplateTags;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.ncedu.blablatruck.domain.entities.TripStatus.*;

@WebServlet("/trip")
public class TripServlet extends HttpServlet {
    private static final String TRIP_AUTH_CONTENT_DELEGATE = "tripAuth";
    private static final String TRIP_AUTH_TITLE = "Мои поездки";
    private TripDAO tripDAO = new HibernateTripDAO();

    @Resource(name = "blablatruck/SessionFactory")
    private SessionFactory sessionFactory;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try (Session session = sessionFactory.openSession()) {
            String filterByStatus = req.getParameter("status") == null ? "CURRENT" : req.getParameter("status");
            Query query;
            switch (filterByStatus) {
                case "COMPLETED":
                    query = session.getNamedQuery(Trip.FIND_ALL_BY_USERNAME_AND_STATUSES)
                            .setParameter("username", req.getRemoteUser())
                            .setParameterList("statuses", COMPLETED);
                    break;
                case "ALL":
                    query = session.getNamedQuery(Trip.FIND_ALL_BY_USERNAME)
                            .setParameter("username", req.getRemoteUser());
                    break;
                case "CURRENT":
                default:
                    query = session.getNamedQuery(Trip.FIND_ALL_BY_USERNAME_AND_STATUSES)
                            .setParameter("username", req.getRemoteUser())
                            .setParameterList("statuses", CURRENT);
            }

            List trips = query.list();
            List<Map<String, Object>> templateData = tripsToTemplate(trips);

            HtmlResponseWriter.create(resp)
                    .setContentDelegate(TRIP_AUTH_CONTENT_DELEGATE)
                    .setTitle(TRIP_AUTH_TITLE)
                    .setCsrfToken((CsrfToken) req.getAttribute("_csrf"))
                    .putData(TemplateTags.TRIPS, templateData)
                    .putData("filterByStatus", filterByStatus)
                    .write();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch (req.getParameter("action")) {
            case "delete_trip":
                deleteTrip(req, resp);
                break;
            case "update_trip_status":
                updateTripStatus(req, resp);
                break;
            case "update_order_status":
                updateOrder(req, resp);
                break;
            default:
                resp.sendError(404);
        }
    }

    private void updateTripStatus(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try (Session session = sessionFactory.openSession()) {
            Trip trip = session.get(Trip.class, Integer.parseInt(req.getParameter("tripId")));
            TripStatus newStatus = TripStatus.valueOf(req.getParameter("newStatus"));
            if (canUpdateStatus(trip, req.getRemoteUser(), newStatus)) {
                trip.setStatus(newStatus);
                session.flush();
                
                doGet(req, resp);
            } else {
                resp.sendError(403);
            }
        }
    }

    private boolean canUpdateStatus(Trip trip, String remoteUser, TripStatus newStatus) {
        if (!trip.getOwner().getUsername().equals(remoteUser))
            return false;

        if (newStatus == FULL && trip.getStatus() == NEW)
            return true;

        if (newStatus == IN_FLIGHT && (trip.getStatus() == NEW || trip.getStatus() == FULL))
            return true;

        if (newStatus == FINISHED && trip.getStatus() == IN_FLIGHT)
            return true;

        long activeOrders = trip.getOrders().stream()
                .filter(order -> order.getStatus() != OrderStatus.STATUS_REJECTED)
                .count();
        if (newStatus == CANCELLED && activeOrders == 0)
            return true;

        return false;
    }

    private void deleteTrip(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Trip trip = tripDAO.get(Integer.parseInt(req.getParameter(TemplateTags.TRIP_ID)));

        if (trip.getOwner().getUsername().equals(SecurityContextHolder.getContext().getAuthentication().getName()))
            tripDAO.delete(trip);
        resp.sendRedirect("/trip");
    }

    private void updateOrder(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try (Session session = sessionFactory.openSession()) {
            Order order = session.get(Order.class, Integer.parseInt(req.getParameter("order_id")));

            OrderStatus newStatus = OrderStatus.valueOf(req.getParameter("new_status"));
            OrderDAO.permitStatusUpdating(order, req.getRemoteUser(), newStatus);

            order.setStatus(newStatus);
            session.flush();

            if (req.getHeader("X-Requested-With").equals("XMLHttpRequest")) {
                resp.getWriter().print(newStatus.toString());
            } else {
                resp.sendRedirect("/trip");
            }
        }
    }

    private List<Map<String, Object>> tripsToTemplate(List<Trip> trips) {
        List<Map<String, Object>> templateTrips = new ArrayList<>();
        for (Trip trip : trips) {
            Map<String, Object> templateTrip = new HashMap<>();
            templateTrip.put(TemplateTags.TRIP_ID, trip.getId());
            templateTrip.put(TemplateTags.TRIP_ARRIVAL, trip.getArrival());
            templateTrip.put(TemplateTags.TRIP_DEPARTURE, trip.getDeparture());
            templateTrip.put(TemplateTags.TRIP_DESCRIPTION, trip.getDescription());
            templateTrip.put(TemplateTags.TRIP_DIMX, trip.getDimX());
            templateTrip.put(TemplateTags.TRIP_DIMY, trip.getDimY());
            templateTrip.put(TemplateTags.TRIP_DIMZ, trip.getDimZ());
            templateTrip.put(TemplateTags.TRIP_PRICE_PER_KG, trip.getPricePerKg());
            templateTrip.put(TemplateTags.TRIP_PRICE_PER_M2, trip.getPricePerM2());
            templateTrip.put(TemplateTags.TRIP_STATUS, trip.getStatus().toString());
            templateTrip.put(TemplateTags.TRIP_TOTAL_MASS, trip.getTotalMass());
            templateTrip.put(TemplateTags.TRIP_DEPARTURE_CITY, trip.getDepartureCity());
            templateTrip.put(TemplateTags.TRIP_ARRIVAL_CITY, trip.getArrivalCity());

            List<Map<String, Object>> orders = new ArrayList<>();
            for (Order order : trip.getOrders()) {
                Map<String, Object> orderInfo = new HashMap<>();

                User user = order.getClient();
                orderInfo.put(TemplateTags.USERNAME, user.getUsername());
                orderInfo.put(TemplateTags.USER_FULLNAME, user.getFirstName() + " " + user.getLastName());
                orderInfo.put(TemplateTags.ORDER_ID, order.getId());
                orderInfo.put(TemplateTags.ORDER_FROM, order.getWpFrom());
                orderInfo.put(TemplateTags.ORDER_TO, order.getWpTo());
                orderInfo.put(TemplateTags.ORDER_DIMX, order.getDimX());
                orderInfo.put(TemplateTags.ORDER_DIMY, order.getDimY());
                orderInfo.put(TemplateTags.ORDER_MASS, order.getMass());
                orderInfo.put(TemplateTags.ORDER_STATUS, order.getStatus().toString());

                orders.add(orderInfo);
            }
            templateTrip.put(TemplateTags.ORDERS, orders);
            templateTrip.put("canCancel", canUpdateStatus(trip, trip.getOwner().getUsername(), CANCELLED));
            templateTrips.add(templateTrip);
        }
        return templateTrips;
    }
}
