package ru.ncedu.blablatruck.servlet;

import ru.ncedu.blablatruck.templateutil.HtmlResponseWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/index.html")
public class IndexServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HtmlResponseWriter.create(resp)
                .setContentDelegate("index")
                .setTitle("Найди перевозку своей мечты")
                .write();
    }
}
