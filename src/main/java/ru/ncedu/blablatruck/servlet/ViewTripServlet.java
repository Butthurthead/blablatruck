package ru.ncedu.blablatruck.servlet;

import org.springframework.security.web.csrf.CsrfToken;
import ru.ncedu.blablatruck.domain.dao.OrderDAO;
import ru.ncedu.blablatruck.domain.dao.TripDAO;
import ru.ncedu.blablatruck.domain.dao.UserDAO;
import ru.ncedu.blablatruck.domain.dao.implementations.HibernateOrderDAO;
import ru.ncedu.blablatruck.domain.dao.implementations.HibernateTripDAO;
import ru.ncedu.blablatruck.domain.dao.implementations.HibernateUserDAO;
import ru.ncedu.blablatruck.domain.entities.Order;
import ru.ncedu.blablatruck.domain.entities.Trip;
import ru.ncedu.blablatruck.domain.entities.User;
import ru.ncedu.blablatruck.form.forms.OrderForm;
import ru.ncedu.blablatruck.form.util.DarkMagicFormFactory;
import ru.ncedu.blablatruck.form.util.DefaultFormDataExtractor;
import ru.ncedu.blablatruck.templateutil.HtmlResponseWriter;
import ru.ncedu.blablatruck.templateutil.TemplateTags;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import java.util.*;

@WebServlet("/viewtrip")
public class ViewTripServlet extends AppHttpServlet {

    private static final String VIEW_TRIP_CONTENT_DELEGATE = "viewTrip";
    private OrderDAO orderDAO = new HibernateOrderDAO();
    private UserDAO userDAO = new HibernateUserDAO();
    private TripDAO tripDAO = new HibernateTripDAO();

    private HtmlResponseWriter createTripView(HttpServletRequest req, HttpServletResponse resp, Trip trip) {
        //until there are no entities for locations and way points
        List<String> locations = tripDAO.waypoints(trip);
        //-----------
        return HtmlResponseWriter.create(resp).setContentDelegate(VIEW_TRIP_CONTENT_DELEGATE)
                .setCsrfToken((CsrfToken)req.getAttribute("_csrf"))
                .setTitle("Просмотр поездки")
                .putData(TemplateTags.TRIP_DESCRIPTION, trip.getDescription())
                .putData(TemplateTags.TRIP_ARRIVAL, trip.getArrival())
                .putData(TemplateTags.TRIP_DEPARTURE, trip.getDeparture())
                .putData(TemplateTags.TRIP_TOTAL_MASS, trip.getTotalMass())
                .putData(TemplateTags.TRIP_DIMX, trip.getDimX())
                .putData(TemplateTags.TRIP_DIMY, trip.getDimY())
                .putData(TemplateTags.TRIP_DIMZ, trip.getDimZ())
                .putData(TemplateTags.TRIP_PRICE_PER_KG, trip.getPricePerKg())
                .putData(TemplateTags.TRIP_PRICE_PER_M2, trip.getPricePerM2())
                .putData(TemplateTags.TRIP_LOCATIONS, locations)
                .putData(TemplateTags.TRIP_OWNER_NAME, trip.getOwner().getUsername());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        try {
            int tripId;
            try {
                tripId = Integer.parseInt(req.getParameter(TemplateTags.TRIP_ID));
            } catch (NumberFormatException nf) {
                resp.sendError(400);
                return;
            }
            Trip trip = tripDAO.get(tripId);
            if (trip == null) {
                resp.sendError(404);
                return;
            }
            OrderForm form = new DarkMagicFormFactory<>(OrderForm.class).createFromEntity(new Order());
            form.setTripId(trip.getId());
            User user = userDAO.get(getAuthUsername());
            HtmlResponseWriter writer = createTripView(req, resp, trip);
            if (user != null) {
                writer.putData(TemplateTags.FORM_DATA,
                        new DefaultFormDataExtractor<>(OrderForm.class).extract(form, null)
                );
            }
            String[] points = req.getParameterValues("points");
            if (points != null) {
                writer.putData(TemplateTags.TRIP_POINTS, Arrays.asList(points));
            }
            writer.write();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        try {
            OrderForm form = new DarkMagicFormFactory<>(OrderForm.class).createFromRequest(req);
            Set<ConstraintViolation<OrderForm>> violations = validator.validate(form);
            if (violations.size() == 0) {
                try {
                    orderDAO.create(form, getAuthUsername());
                    resp.sendRedirect("/orders");
                } catch (SecurityException se) {
                    resp.sendError(403);
                } catch (IllegalArgumentException e) {
                    resp.sendError(404);
                }
            } else {
                Trip trip = tripDAO.get(Integer.parseInt(form.getTripId().toString()));
                if (trip == null) {
                    resp.sendError(404);
                    return;
                }
                Map<String, Object> formData =
                        new DefaultFormDataExtractor<>(OrderForm.class).extract(form, violations);
                createTripView(req, resp, trip).putData(TemplateTags.FORM_DATA, formData).write();
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}
