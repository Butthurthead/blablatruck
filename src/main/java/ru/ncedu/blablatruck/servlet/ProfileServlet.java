package ru.ncedu.blablatruck.servlet;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.csrf.CsrfToken;
import ru.ncedu.blablatruck.domain.dao.UserDAO;
import ru.ncedu.blablatruck.domain.dao.implementations.HibernateUserDAO;
import ru.ncedu.blablatruck.domain.entities.Comment;
import ru.ncedu.blablatruck.domain.entities.User;
import ru.ncedu.blablatruck.form.forms.CommentForm;
import ru.ncedu.blablatruck.form.forms.ProfileForm;
import ru.ncedu.blablatruck.form.forms.groups.UniqueEmailGroup;
import ru.ncedu.blablatruck.form.util.DarkMagicFormFactory;
import ru.ncedu.blablatruck.form.util.DefaultFormDataExtractor;
import ru.ncedu.blablatruck.templateutil.HtmlResponseWriter;
import ru.ncedu.blablatruck.templateutil.TemplateTags;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.groups.Default;
import java.io.IOException;
import java.util.*;

@WebServlet("/profile")
public class ProfileServlet extends AppHttpServlet {

    protected static final String PROFILE_AUTH_CONTENT_DELEGATE = "profileAuth";
    protected static final String PROFILE_UNAUTH_CONTENT_DELEGATE = "profileUnauth";
    protected UserDAO userDAO = new HibernateUserDAO();
    protected static final String PROFILE_AUTH_TITLE = "Мой профиль";

    protected void putUserData(HtmlResponseWriter writer, User user) {
        writer.putData(TemplateTags.USERNAME, user.getUsername());
        writer.putData(TemplateTags.PHONE, user.getPhone());
        writer.putData(TemplateTags.EMAIL, user.getEmail());
        writer.putData(TemplateTags.FIRST_NAME, user.getFirstName());
        writer.putData(TemplateTags.LAST_NAME, user.getLastName());
        writer.putData(TemplateTags.PROFILE_COMMENTS, commentsToTemplate(user.getReceivedComments()));
    }

    private List<Map<String, String>> commentsToTemplate(List<Comment> comments) throws NullPointerException {
        List<Map<String, String>> templateComments = new ArrayList<>();
        for (Comment comment : comments) {
            Map<String, String> templateComment = new HashMap<>();
            templateComment.put(TemplateTags.PROFILE_COMMENT_AUTHOR, comment.getAuthor().getUsername());
            templateComment.put(TemplateTags.PROFILE_COMMENT_TEXT, comment.getText());
            templateComment.put(TemplateTags.PROFILE_COMMENT_RECEIVE_DATE, comment.getReceiveDate().toString());
            templateComments.add(templateComment);
        }
        return templateComments;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException  {
        String usernameFromReq = req.getParameter(TemplateTags.USERNAME);
        String authUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Boolean authorized = (usernameFromReq == null);
        String actualUsername = authorized ? authUsername : usernameFromReq;
        User user = userDAO.get(actualUsername);
        if (user == null) {
            if (authorized)
                resp.sendError(403);
            else
                resp.sendError(404);
            return;
        }

        Map<String, Object> formData;
        HtmlResponseWriter writer = HtmlResponseWriter.create(resp);
        try {
            if (authorized) {
                ProfileForm form = new DarkMagicFormFactory<>(ProfileForm.class).createFromEntity(user);
                formData = new DefaultFormDataExtractor<>(ProfileForm.class).extract(form, null);
            } else {
                putUserData(writer, user);
                CommentForm commentForm = new CommentForm();
                commentForm.setReceiver(actualUsername);
                formData = new DefaultFormDataExtractor<>(CommentForm.class).extract(commentForm, null);
            }
        } catch (Exception e) {
            logStackTrace(e);
            throw new ServletException(e);
        }
        writer.setCsrfToken((CsrfToken) req.getAttribute("_csrf"))
                .setTitle(authorized ? PROFILE_AUTH_TITLE : actualUsername)
                .setContentDelegate(authorized ? PROFILE_AUTH_CONTENT_DELEGATE : PROFILE_UNAUTH_CONTENT_DELEGATE)
                .putData(TemplateTags.FORM_DATA, formData)
                .write();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        try {
            ProfileForm form = new DarkMagicFormFactory<>(ProfileForm.class).
                    createFromRequest(req);
            boolean emailChanged = !userDAO.get(username).getEmail().equals(form.getEmail());
            Set<ConstraintViolation<ProfileForm>> violations = emailChanged ?
                    validator.validate(form, Default.class, UniqueEmailGroup.class) :
                    validator.validate(form, Default.class);
            if (violations.size() == 0) {
                userDAO.update(username, form);
            }
            HtmlResponseWriter.create(resp)
                    .setCsrfToken((CsrfToken) req.getAttribute("_csrf"))
                    .setTitle(PROFILE_AUTH_TITLE)
                    .setContentDelegate(PROFILE_AUTH_CONTENT_DELEGATE)
                    .putData(TemplateTags.FORM_DATA,
                            new DefaultFormDataExtractor<>(ProfileForm.class).extract(form, violations))
                    .write();
        } catch (Exception e) {
            logStackTrace(e);
            throw new ServletException(e);
        }
    }

}
