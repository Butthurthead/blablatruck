package ru.ncedu.blablatruck.servlet;

import org.springframework.security.web.csrf.CsrfToken;
import ru.ncedu.blablatruck.domain.entities.User;
import ru.ncedu.blablatruck.form.forms.CommentForm;
import ru.ncedu.blablatruck.form.util.DarkMagicFormFactory;
import ru.ncedu.blablatruck.form.util.DefaultFormDataExtractor;
import ru.ncedu.blablatruck.form.util.FormFactory;
import ru.ncedu.blablatruck.templateutil.HtmlResponseWriter;
import ru.ncedu.blablatruck.templateutil.TemplateTags;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.util.Set;

@WebServlet("/postcomment")
public class CommentServlet extends ProfileServlet {

    private FormFactory<CommentForm> formFactory = new DarkMagicFormFactory<>(CommentForm.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendError(404);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        try {
            String receiverUsername = getAuthUsername();
            CommentForm form = formFactory.createFromRequest(req);
            Set<ConstraintViolation<CommentForm>> violations = validator.validate(form);
            if (violations.size() == 0) {
                try {
                    userDAO.postComment(receiverUsername, form);
                    resp.sendRedirect("/profile?username=" + form.getReceiver());
                } catch (SecurityException se) {
                    resp.sendError(403);
                } catch (IllegalArgumentException ia) {
                    resp.sendError(404);
                }
            } else {
                User receiver = userDAO.get(form.getReceiver());
                if (receiver == null)
                    resp.sendError(404);
                HtmlResponseWriter writer = HtmlResponseWriter.create(resp)
                        .setContentDelegate(PROFILE_UNAUTH_CONTENT_DELEGATE)
                        .setTitle(receiverUsername)
                        .setCsrfToken((CsrfToken) req.getAttribute("_csrf"))
                        .putData(TemplateTags.FORM_DATA,
                                new DefaultFormDataExtractor<>(CommentForm.class).extract(form, violations));
                putUserData(writer, receiver);
                writer.write();
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

}
