package ru.ncedu.blablatruck.servlet;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.csrf.CsrfToken;
import ru.ncedu.blablatruck.domain.dao.LocationDAO;
import ru.ncedu.blablatruck.domain.dao.TripDAO;
import ru.ncedu.blablatruck.domain.dao.implementations.HibernateLocationDAO;
import ru.ncedu.blablatruck.domain.dao.implementations.HibernateTripDAO;
import ru.ncedu.blablatruck.domain.dao.implementations.HibernateUserDAO;
import ru.ncedu.blablatruck.domain.entities.Location;
import ru.ncedu.blablatruck.domain.entities.RouteWaypoint;
import ru.ncedu.blablatruck.domain.entities.Trip;
import ru.ncedu.blablatruck.domain.entities.User;
import ru.ncedu.blablatruck.form.forms.TripForm;
import ru.ncedu.blablatruck.form.util.DarkMagicFormFactory;
import ru.ncedu.blablatruck.form.util.DefaultFormDataExtractor;
import ru.ncedu.blablatruck.form.util.FormFactory;
import ru.ncedu.blablatruck.form.util.FormUtil;
import ru.ncedu.blablatruck.templateutil.HtmlResponseWriter;
import ru.ncedu.blablatruck.templateutil.TemplateTags;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@WebServlet("/createtrip")
public class CreateTripServlet extends AppHttpServlet {
    private static final String CREATE_TRIP = "createTripAuth";
    protected static final String CREATE_TRIP_TITLE = "Создать поездку";

    private FormFactory<TripForm> tripFormFactory;
    private TripDAO tripDAO = new HibernateTripDAO();
    private LocationDAO locationDAO = new HibernateLocationDAO();

    @Override
    public void init() throws ServletException {
        tripFormFactory = new DarkMagicFormFactory<>(TripForm.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String actualUsername = SecurityContextHolder.getContext().getAuthentication().getName();

        Map<String, Object> formData;
        HtmlResponseWriter writer = HtmlResponseWriter.create(resp);

        if (actualUsername == null) {
            resp.sendError(403);
        }

        try {
            TripForm form = new TripForm();
            formData = new DefaultFormDataExtractor<>(TripForm.class).extract(form, null);
            formData.put(TemplateTags.USERNAME, actualUsername);
        } catch (Exception e) {
            throw new ServletException(e);
        }
        writer.setCsrfToken((CsrfToken) req.getAttribute("_csrf"))
                .setTitle(CREATE_TRIP_TITLE)
                .setContentDelegate(CREATE_TRIP)
                .putData(TemplateTags.FORM_DATA, formData)
                .write();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TripForm form;
        User user = (new HibernateUserDAO()).get(getAuthUsername());
        if (user == null) {
            resp.sendError(403);
            return;
        }
        try {
            form = tripFormFactory.createFromRequest(req);
            Set<ConstraintViolation<TripForm>> violations = validator.validate(form);
            if (violations.size() == 0) {
                Trip trip = new Trip();
                FormUtil.updateEntity(trip, form);
                trip.setOwner(user);
                List<Location> locations = new ArrayList<>();
                Location departureCity = locationDAO.get(trip.getDepartureCity());
                if (departureCity == null) {
                    Location l = new Location();
                    l.setName(trip.getDepartureCity());
                    locationDAO.create(l);
                    locations.add(locationDAO.get(trip.getDepartureCity()));
                } else locations.add(departureCity);
                if (req.getParameter(TemplateTags.VIA_POINTS) != null) {
                    for (String s : req.getParameter(TemplateTags.VIA_POINTS).split(",")) {
                        Location exists = locationDAO.get(s);
                        if (exists == null) {
                            Location l = new Location();
                            l.setName(s);
                            locationDAO.create(l);
                            locations.add(locationDAO.get(s));
                        } else {
                            locations.add(exists);
                        }
                    }
                }
                Location arrivalCity = locationDAO.get(trip.getArrivalCity());
                if (arrivalCity == null) {
                    Location l = new Location();
                    l.setName(trip.getArrivalCity());
                    locationDAO.create(l);
                    locations.add(locationDAO.get(trip.getArrivalCity()));
                } else locations.add(arrivalCity);

                List<RouteWaypoint> routeWaypoints = new ArrayList<>();
                for (int i = 0; i < locations.size(); i++) {
                    RouteWaypoint r = new RouteWaypoint();
                    r.setLocation(locations.get(i));
                    r.setPosition(i + 1);
                    routeWaypoints.add(r);
                }
                tripDAO.create(trip, routeWaypoints);
                resp.sendRedirect("/trip");
            } else {
                HtmlResponseWriter.create(resp)
                        .setContentDelegate(CREATE_TRIP)
                        .setTitle(CREATE_TRIP_TITLE)
                        .setCsrfToken((CsrfToken) req.getAttribute("_csrf"))
                        .putData(TemplateTags.FORM_DATA,
                                new DefaultFormDataExtractor<>(TripForm.class).extract(form, violations))
                        .write();
            }
        } catch (SecurityException sec) {
            resp.sendError(403);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

}
