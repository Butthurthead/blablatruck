package ru.ncedu.blablatruck.servlet;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.csrf.CsrfToken;
import ru.ncedu.blablatruck.domain.dao.OrderDAO;
import ru.ncedu.blablatruck.domain.dao.implementations.HibernateOrderDAO;
import ru.ncedu.blablatruck.domain.entities.Order;
import ru.ncedu.blablatruck.form.forms.UpdateOrderForm;
import ru.ncedu.blablatruck.form.util.DarkMagicFormFactory;
import ru.ncedu.blablatruck.form.util.DefaultFormDataExtractor;
import ru.ncedu.blablatruck.templateutil.HtmlResponseWriter;
import ru.ncedu.blablatruck.templateutil.TemplateTags;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/orders")
public class OrderServlet extends AppHttpServlet {
    private static final String CLIENT_ORDERS_CONTENT_DELEGATE = "clientOrders";
    private static final String CLIENT_ORDERS_TITLE = "Мои заказы";
    private OrderDAO orderDAO = new  HibernateOrderDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String actualUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        HtmlResponseWriter writer = HtmlResponseWriter.create(resp);

        if (actualUsername == null){
            resp.sendError(403);
        }
        try {
            List<Map<String, Object>> ordersToTemplate = ordersToTemplate(
                    orderDAO.getOrderListByUsername(actualUsername), actualUsername);
            writer.putData(TemplateTags.USERNAME, actualUsername);
            writer.putData(TemplateTags.ORDERS, ordersToTemplate);
        } catch (Exception e) {
            throw new ServletException(e);
        }
        writer.setCsrfToken((CsrfToken) req.getAttribute("_csrf"))
                .setTitle(CLIENT_ORDERS_TITLE)
                .setContentDelegate(CLIENT_ORDERS_CONTENT_DELEGATE)
                .write();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        String actualUsername = SecurityContextHolder.getContext().getAuthentication().getName();

        if (actualUsername == null){
            resp.sendError(403);
        }

        String deleteId = req.getParameter(TemplateTags.ORDER_ID_FOR_DELETE);
        if (deleteId != null) {
            try {
                Integer orderIdForDelete = Integer.valueOf(deleteId);
                orderDAO.delete(orderDAO.get(orderIdForDelete), actualUsername);
                resp.setStatus(200);
            } catch (NumberFormatException nf) {
                resp.sendError(400);
            } catch (SecurityException s) {
                resp.sendError(403);
            } catch (Exception e) {
                throw new ServletException(e);
            }
        } else {
            try {
                UpdateOrderForm form = new DarkMagicFormFactory<>(UpdateOrderForm.class).createFromRequest(req);
                orderDAO.update(form, actualUsername);
                Order updatedOrder = orderDAO.get(form.getOrderId());
                form.initStatusOptions(updatedOrder, actualUsername);
                HtmlResponseWriter.create(resp)
                        .setCsrfToken((CsrfToken) req.getAttribute("_csrf"))
                        .setLayout("clientOrders.updateOrder")
                        .putData(TemplateTags.FORM_DATA,
                                new DefaultFormDataExtractor<>(UpdateOrderForm.class).extract(form, null))
                        .putData(TemplateTags.ORDER_STATUS, form.getStatus())
                        .write();
            } catch (IllegalArgumentException ia) {
                resp.sendError(404);
            } catch (SecurityException | UnsupportedOperationException se) {
                resp.sendError(403);
            } catch (Exception e){
                throw new ServletException(e);
            }
        }
    }

    private List<Map<String, Object>> ordersToTemplate(List<Order> orders, String username) throws Exception {

        orders = orders == null ? new ArrayList<>() : orders;

        List<Map<String, Object>> templateOrders = new ArrayList<>();
        for (Order order : orders) {
            Map<String, Object> templateOrder = new HashMap<>();
            templateOrder.put(TemplateTags.ORDER_ID, order.getId());
            templateOrder.put(TemplateTags.ORDER_DIMX, order.getDimX());
            templateOrder.put(TemplateTags.ORDER_FROM, order.getWpFrom());
            templateOrder.put(TemplateTags.ORDER_TO, order.getWpTo());
            templateOrder.put(TemplateTags.ORDER_DIMY, order.getDimY());
            templateOrder.put(TemplateTags.ORDER_MASS, order.getMass());
            templateOrder.put(TemplateTags.ORDER_STATUS, order.getStatus().toString());
            templateOrder.put(TemplateTags.ORDER_CAN_DELETE, OrderDAO.deletionPermitted(order, username));

            templateOrder.put(TemplateTags.TRIP_ID, order.getTrip().getId());
            templateOrder.put(TemplateTags.TRIP_DEPARTURE_CITY, order.getTrip().getDepartureCity());
            templateOrder.put(TemplateTags.TRIP_DEPARTURE, order.getTrip().getDeparture());
            templateOrder.put(TemplateTags.TRIP_ARRIVAL_CITY, order.getTrip().getArrivalCity());
            templateOrder.put(TemplateTags.TRIP_ARRIVAL, order.getTrip().getArrival());
            templateOrder.put(TemplateTags.TRIP_OWNER_NAME, order.getTrip().getOwner().getUsername());

            UpdateOrderForm form = new DarkMagicFormFactory<>(UpdateOrderForm.class).createFromEntity(order);
            form.initStatusOptions(order, username);
            templateOrder.put(TemplateTags.FORM_DATA,
                    new DefaultFormDataExtractor<>(UpdateOrderForm.class).extract(form, null));
            templateOrders.add(templateOrder);
        }
        return templateOrders;
    }
}
