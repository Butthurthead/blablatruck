package ru.ncedu.blablatruck.domain.dao;

import ru.ncedu.blablatruck.domain.entities.Order;
import ru.ncedu.blablatruck.form.forms.OrderForm;
import ru.ncedu.blablatruck.form.forms.UpdateOrderForm;

import java.util.List;

public interface OrderDAO {

    Order get(Integer id);

    Order create(OrderForm orderForm, String username);

    void update(UpdateOrderForm form, String username);

    void delete(Order order, String username);

    List<Order> getOrderListByUsername(String username);

    static void permitStatusUpdating(Order order, String username, OrderStatus newStatus)
            throws SecurityException, UnsupportedOperationException {
        boolean isOwner = false;
        if (username.equals(order.getTrip().getOwner().getUsername())) {
            isOwner = true;
        } else if (!username.equals(order.getClient().getUsername())) {
            throw new SecurityException();
        }
        if (order.getStatus().equals(newStatus))
            return;
        switch (order.getStatus()) {
            case STATUS_NEW:
                if (isOwner &&
                        (newStatus.equals(OrderStatus.STATUS_COMMITTED) || newStatus.equals(OrderStatus.STATUS_REJECTED)) ) {
                    break;
                }
                throw new UnsupportedOperationException();
            case STATUS_COMMITTED:
                if (newStatus.equals(OrderStatus.STATUS_REJECTED) ||
                        (!isOwner && newStatus.equals(OrderStatus.STATUS_SHIPPED)) ) {
                    break;
                }
                throw new UnsupportedOperationException();
            case STATUS_SHIPPED:
                if (!isOwner && newStatus.equals(OrderStatus.STATUS_DELIVERED)) {
                    break;
                }
                throw new UnsupportedOperationException();
            default:
                throw new UnsupportedOperationException();
        }
    }

    static boolean deletionPermitted(Order order, String username) {
        return (order.getClient().getUsername().equals(username) && order.getStatus().equals(OrderStatus.STATUS_NEW));
    }

}
