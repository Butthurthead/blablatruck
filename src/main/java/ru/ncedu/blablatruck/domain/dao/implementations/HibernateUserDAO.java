package ru.ncedu.blablatruck.domain.dao.implementations;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ru.ncedu.blablatruck.domain.dao.UserDAO;
import ru.ncedu.blablatruck.domain.entities.Comment;
import ru.ncedu.blablatruck.domain.entities.Role;
import ru.ncedu.blablatruck.domain.entities.User;
import ru.ncedu.blablatruck.domain.util.HibernateUtil;
import ru.ncedu.blablatruck.form.forms.CommentForm;
import ru.ncedu.blablatruck.form.forms.ProfileForm;
import ru.ncedu.blablatruck.form.forms.RegistrationForm;
import ru.ncedu.blablatruck.form.util.FormUtil;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Date;

public class HibernateUserDAO implements UserDAO {

    private static final int DEFAULT_ROLE_ID = 1;
    private SessionFactory sessionFactory;

    public HibernateUserDAO() {
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            sessionFactory = (SessionFactory) envContext.lookup("blablatruck/SessionFactory");
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public User get(String username) {
        try (Session session = sessionFactory.openSession()) {
            return getByUsername(session, username);
        }
    }

    @Override
    public void create(RegistrationForm form) {
        try (Session session = sessionFactory.openSession()) {
            User user = new User();
            FormUtil.updateEntity(user, form);
            user.setPassword(form.getPassword());
            user.setRole(session.get(Role.class, DEFAULT_ROLE_ID));
            session.save(user);
        } catch (IllegalAccessException | InstantiationException | NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(String username, ProfileForm form) {
        try (Session session = sessionFactory.openSession()) {
            User user = getByUsername(session, username);
            FormUtil.updateEntity(user, form);
            if (form.getPassword() != null && !form.getPassword().equals("")) {
                user.setPassword(form.getPassword());
            }
            session.flush();
        } catch (IllegalAccessException | InstantiationException | NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Use this method inside an other HibernateDAO to select user from current session.
     * @param session current session
     * @param username username
     * @return selected User instance or null if there is no user with such username
     */
    public static User getByUsername(Session session, String username) {
        return (User) session
                .createQuery("select u from User u where u.username = :username")
                .setParameter("username", username)
                .uniqueResult();
    }

    @Override
    public void postComment(String username, CommentForm commentForm) throws SecurityException, IllegalArgumentException {
        HibernateUtil.inTransaction(sessionFactory, session -> {
            try {
                User sender = getByUsername(session, username);
                if (sender == null)
                    throw new SecurityException();
                User receiver = getByUsername(session, commentForm.getReceiver());
                if (receiver == null)
                    throw new IllegalArgumentException();

                Comment comment = new Comment();
                FormUtil.updateEntity(comment, commentForm);
                comment.setAuthor(sender);
                comment.setUser(receiver);
                comment.setReceiveDate(new Date());

                session.save(comment);
            } catch (IllegalAccessException | InstantiationException | NoSuchFieldException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public boolean isUsernameUnique(String username) {
        return get(username) == null;
    }

    @Override
    public boolean isEmailUnique(String email) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        User user = (User) session.createQuery("select u from User u where u.email = :email")
                    .setParameter("email", email).uniqueResult();
        session.getTransaction().commit();
        return user == null;
    }
}
