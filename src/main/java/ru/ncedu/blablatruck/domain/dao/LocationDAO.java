package ru.ncedu.blablatruck.domain.dao;

import ru.ncedu.blablatruck.domain.entities.Location;
import ru.ncedu.blablatruck.domain.entities.Trip;
import ru.ncedu.blablatruck.form.forms.FindTripForm;
import ru.ncedu.blablatruck.form.forms.TripForm;

import java.util.List;

public interface LocationDAO {

    Location get(int id);
    Location get(String name);
    void create(Location l);
    void delete(Location l);
    List<String> found(String regexp);
}
