package ru.ncedu.blablatruck.domain.dao.implementations;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ru.ncedu.blablatruck.domain.dao.LocationDAO;
import ru.ncedu.blablatruck.domain.entities.Location;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.List;

public class HibernateLocationDAO implements LocationDAO {

    private SessionFactory sessionFactory;

    public HibernateLocationDAO() {
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            sessionFactory = (SessionFactory) envContext.lookup("blablatruck/SessionFactory");
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Location get(int id) {
        try (Session session = sessionFactory.openSession()) {
            return (Location) session
                    .createQuery("from " + Location.class.getCanonicalName() + " where id = :id")
                    .setParameter("id", id)
                    .uniqueResult();
        }
    }

    @Override
    public Location get(String name) {
        try (Session session = sessionFactory.openSession()) {
            return (Location) session
                    .createQuery("from " + Location.class.getCanonicalName() + " where name = :name")
                    .setParameter("name", name)
                    .uniqueResult();
        }
    }

    @Override
    public void create(Location l) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(l);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Location l) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(l);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<String> found(String regexp) {
        try (Session session = sessionFactory.openSession()) {
            return  session
                    .createQuery("select name from " + Location.class.getCanonicalName() + " where LOWER(name) like :regexp")
                    .setParameter("regexp", "%" + regexp.toLowerCase() + "%")
                    .list();
        }
    }
}
