package ru.ncedu.blablatruck.domain.dao.implementations;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ru.ncedu.blablatruck.domain.dao.TripDAO;
import ru.ncedu.blablatruck.domain.entities.Location;
import ru.ncedu.blablatruck.domain.entities.RouteWaypoint;
import ru.ncedu.blablatruck.domain.entities.Trip;
import ru.ncedu.blablatruck.domain.entities.User;
import ru.ncedu.blablatruck.domain.util.HibernateUtil;
import ru.ncedu.blablatruck.form.forms.FindTripForm;
import ru.ncedu.blablatruck.form.forms.TripForm;
import ru.ncedu.blablatruck.form.util.FormUtil;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mary on 04.04.16.
 */
public class HibernateTripDAO implements TripDAO {


    private SessionFactory sessionFactory;

    public HibernateTripDAO() {
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            sessionFactory = (SessionFactory) envContext.lookup("blablatruck/SessionFactory");
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Trip get(int id) {
        try (Session session = sessionFactory.openSession()) {
            return getById(session, id);
        }
    }

    @Override
    public void create(TripForm form, String username) throws SecurityException {
        HibernateUtil.inTransaction(sessionFactory, session -> {
            User user = HibernateUserDAO.getByUsername(session, username);
            if (user == null)
                throw new SecurityException();
            Trip trip = new Trip();
            try {
                FormUtil.updateEntity(trip, form);
            } catch (ReflectiveOperationException e) {
                throw new RuntimeException(e);
            }
            trip.setOwner(user);
            session.save(trip);
        });
    }

    @Override
    public void create(Trip trip, List<RouteWaypoint> wayPoints) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            {
                session.save(trip);
                for (RouteWaypoint r : wayPoints){
                    r.setTrip(trip);
                    session.save(r);
                }
            }
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Trip trip) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            {
                session.delete(trip);
            }
            session.getTransaction().commit();
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Trip> getTripListByUsername(String username) {
        try(Session session =sessionFactory.openSession()){
            Query query = session.createQuery("from " + Trip.class.getCanonicalName() + " where owner_id = " +
                    "(select id from " + User.class.getCanonicalName() + " where username = :username) order by Trip.departure");
            query.setParameter("username", username);

            List<Trip> queryResult = query.list();

            return queryResult.isEmpty() ? null : queryResult;
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    private Trip getTripByDesc(Trip t) {
        try(Session session =sessionFactory.openSession()){
            return (Trip) session.createQuery("select t from Trip t " +
                    "where t.description = :description and " +
                    "t.departure = :departure and " +
                    "t.arrival = :arrival and " +
                    "t.totalMass = :totalMass and " +
                    "t.dimX = :dimX and " +
                    "t.dimY = :dimY and " +
                    "t.dimZ = :dimZ and " +
                    "t.pricePerKg = :pricePerKg and " +
                    "t.pricePerM2 = :pricePerM2 and " +
                    "t.owner = :owner")
                    .setParameter("description", t.getDescription())
                    .setParameter("departure", t.getDateDeparture())
                    .setParameter("arrival", t.getDateArrival())
                    .setParameter("totalMass", t.getTotalMass())
                    .setParameter("dimX", t.getDimX())
                    .setParameter("dimY", t.getDimY())
                    .setParameter("dimZ", t.getDimZ())
                    .setParameter("pricePerKg", t.getPricePerKg())
                    .setParameter("pricePerM2", t.getPricePerM2())
                    .setParameter("owner", t.getOwner())
                    .uniqueResult();
        }
    }

    /** Method return trips from lastId and older
     *   @param findTripForm form with filter params;
     *  @param lastId id of last returned trip.
     *                in first time opening page or add filters lastId == -1
     *  @param countRows count returned rows. Defined on TemplateTags.TRIPS_ON_PAGE
     *  @return list of trips
     */
    @Override
    public List<Trip> findTripList(FindTripForm findTripForm, Integer lastId, Integer countRows) {
        try(Session session =sessionFactory.openSession()){

            Query query;
            if (findTripForm.getArrivalCity().equals("") && findTripForm.getDepartureCity().equals("")) {
                query = session.createQuery(
                        "from Trip t where " +
                        "t.totalMass >= :totalMass and " +
                        "t.dimX*t.dimY*t.dimZ >= :totalV " +
                        (lastId != -1 ? "and t.id < :lastId " : "") +
                        "order by t.id desc");
            } else {
                query = session.createQuery(
                        "select distinct t from Trip as t " +
                                "inner join t.routeWaypoints as rw1 " +
                                "inner join rw1.location as l1 " +
                                "inner join t.routeWaypoints as rw2 " +
                                "inner join rw2.location as l2 " +
                                "where l1.name like :departure and l2.name like :arrival and rw2.position > rw1.position and " +
                                "t.totalMass >= :totalMass and " +
                                "t.dimX*t.dimY*t.dimZ >= :totalV " +
                                (lastId != -1 ? "and t.id < :lastId " : "") +
                                "order by t.id desc"
                );
                query.setParameter("arrival", "%" + findTripForm.getArrivalCity() + "%");
                query.setParameter("departure", "%" + findTripForm.getDepartureCity() + "%");
            }
            query.setParameter("totalMass", findTripForm.getTotalMass());
            query.setParameter("totalV", findTripForm.getTotalV());
            if (lastId != -1)
                query.setParameter("lastId", lastId);
            query.setMaxResults(countRows);

            List<Trip> queryResult = query.list();

            return queryResult.isEmpty() ? new ArrayList<>() : queryResult;
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<String> waypoints(Trip t) {
        try (Session session = sessionFactory.openSession()) {
            return (List<String>) session
                    .createQuery("select l.name from Location l " +
                            "inner join RouteWaypoint r on l.id = r.location.id " +
                            "where r.trip = :trip  order by r.position")
                    .setParameter("trip", t)
                    .list();
        }
    }

    private Trip getById(Session session, int id) {
        return (Trip) session
                .createQuery("from " + Trip.class.getCanonicalName() + " where id = :id").
                 setParameter("id", id)
                .uniqueResult();
    }
}
