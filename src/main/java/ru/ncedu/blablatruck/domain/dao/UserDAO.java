package ru.ncedu.blablatruck.domain.dao;

import ru.ncedu.blablatruck.domain.entities.User;
import ru.ncedu.blablatruck.form.forms.CommentForm;
import ru.ncedu.blablatruck.form.forms.ProfileForm;
import ru.ncedu.blablatruck.form.forms.RegistrationForm;

public interface UserDAO {

    User get(String username);
    void create(RegistrationForm form);
    void update(String username, ProfileForm form);
    void postComment(String username, CommentForm commentForm);
    boolean isUsernameUnique(String username);
    boolean isEmailUnique(String email);
}
