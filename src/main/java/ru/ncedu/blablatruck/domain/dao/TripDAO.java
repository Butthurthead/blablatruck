package ru.ncedu.blablatruck.domain.dao;

import ru.ncedu.blablatruck.domain.entities.Location;
import ru.ncedu.blablatruck.domain.entities.RouteWaypoint;
import ru.ncedu.blablatruck.domain.entities.Trip;
import ru.ncedu.blablatruck.form.forms.FindTripForm;
import ru.ncedu.blablatruck.form.forms.TripForm;

import java.util.List;

public interface TripDAO {

    Trip get(int id);
    void create(TripForm form, String username);
    void create(Trip trip, List<RouteWaypoint> way);
    void delete (Trip trip);
    List<Trip> getTripListByUsername(String username);
    List<Trip> findTripList(FindTripForm findTripForm, Integer lastId, Integer count);
    List<String> waypoints(Trip t);
}
