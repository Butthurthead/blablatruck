package ru.ncedu.blablatruck.domain.dao.implementations;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ru.ncedu.blablatruck.domain.dao.OrderDAO;
import ru.ncedu.blablatruck.domain.dao.OrderStatus;
import ru.ncedu.blablatruck.domain.entities.Order;
import ru.ncedu.blablatruck.domain.entities.Trip;
import ru.ncedu.blablatruck.domain.entities.User;
import ru.ncedu.blablatruck.domain.util.HibernateUtil;
import ru.ncedu.blablatruck.form.forms.OrderForm;
import ru.ncedu.blablatruck.form.forms.UpdateOrderForm;
import ru.ncedu.blablatruck.form.util.FormUtil;

import java.util.List;

public class HibernateOrderDAO implements OrderDAO {
    private SessionFactory sessionFactory;

    public HibernateOrderDAO() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    @Override
    public Order get(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            return (Order)session
                    .createQuery("select o from " + Order.class.getCanonicalName() + " o where o.id = :id")
                    .setParameter("id", id)
                    .uniqueResult();
        }
    }

    @Override
    public Order create(OrderForm orderForm, String username) throws SecurityException, IllegalArgumentException {
        Order order = new Order();
        HibernateUtil.inTransaction(sessionFactory, session -> {
            try {
                User user = (User)session.createQuery("select u from " + User.class.getCanonicalName() +
                        " u where u.username = :username").setParameter("username", username).uniqueResult();
                if (user == null)
                    throw new SecurityException("No user found with username " + username);

                Trip trip = session.get(Trip.class, orderForm.getTripId());
                if (trip == null)
                    throw new IllegalArgumentException("No trip found with id = " + orderForm.getTripId());
                if (trip.getOwner().getUsername().equals(username))
                    throw new SecurityException("Owner can't be client");

                FormUtil.updateEntity(order, orderForm);
                order.setTrip(trip);
                order.setClient(user);
                order.setStatus(OrderStatus.STATUS_NEW);
                session.save(order);
            } catch (ReflectiveOperationException e) {
                throw new RuntimeException(e);
            }
        });
        return order;
    }

    @Override
    public void update(UpdateOrderForm form, String username)
            throws SecurityException, UnsupportedOperationException, IllegalArgumentException {
        HibernateUtil.inTransaction(sessionFactory, session -> {
            Order order = session.get(Order.class, form.getOrderId());
            if (order == null)
                throw new IllegalArgumentException("no order found with id = " + form.getOrderId());
            OrderStatus newStatus = OrderStatus.valueOf(form.getStatus());
            OrderDAO.permitStatusUpdating(order, username, newStatus);
            order.setStatus(newStatus);
            session.update(order);
        });
    }

    @Override
    public void delete(Order order, String username) throws SecurityException {
        if (OrderDAO.deletionPermitted(order, username)) {
            HibernateUtil.inTransaction(sessionFactory, session -> session.delete(order));
        } else {
            throw new SecurityException();
        }
    }

    @Override
    public List<Order> getOrderListByUsername(String username) {
        try(Session session = sessionFactory.openSession()){
            Query query = session.createQuery("from Order where client_id = " +
                    "(select id from User where username = :username) order by trip.departure, status");
            query.setParameter("username", username);

            List<Order> queryResult = query.list();

            return queryResult.isEmpty() ? null : queryResult;
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

}
