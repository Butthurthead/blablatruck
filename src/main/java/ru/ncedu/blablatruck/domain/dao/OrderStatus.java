package ru.ncedu.blablatruck.domain.dao;

public enum OrderStatus {
    STATUS_NEW,
    STATUS_COMMITTED,
    STATUS_SHIPPED,
    STATUS_DELIVERED,
    STATUS_REJECTED
}
