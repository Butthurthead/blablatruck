package ru.ncedu.blablatruck.domain.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "trips")
@NamedQueries({
        @NamedQuery(
                name = Trip.FIND_ALL_BY_USERNAME,
                query = "SELECT t FROM Trip t WHERE t.owner.username = :username ORDER BY t.departure ASC"
        ),
        @NamedQuery(
                name = Trip.FIND_ALL_BY_USERNAME_AND_STATUSES,
                query = "SELECT t FROM Trip t WHERE t.owner.username = :username AND t.status IN (:statuses)" +
                        " ORDER BY t.departure ASC"
        ),
})
public class Trip implements Serializable {

    public static final String FIND_ALL_BY_USERNAME = "Trip.findAllByUsername";
    public static final String FIND_ALL_BY_USERNAME_AND_STATUSES = "Trip.findAllByUsernameAndStatuses";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 1024)
    private String description;

    @Enumerated(EnumType.STRING)
    private TripStatus status = TripStatus.NEW;

    private Date departure;

    private Date arrival;

    private Double totalMass;

    private Double dimX;

    private Double dimY;

    private Double dimZ;

    private Double pricePerKg;

    private Double pricePerM2;

    private String departureCity;

    private String arrivalCity;

    @Transient
    private DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");

    @OneToMany(mappedBy = "trip")
    private List<Order> orders = new ArrayList<>();

    @OneToMany(mappedBy = "trip", cascade = CascadeType.REMOVE)
    private List<RouteWaypoint> routeWaypoints = new ArrayList<>();

    @ManyToOne
    private User owner;

    public Trip() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TripStatus getStatus() {
        return status;
    }

    public void setStatus(TripStatus status) {
        this.status = status;
    }

    public String getDeparture() {
        return dateFormat.format(departure);
    }

    public Date getDateDeparture(){
        return departure;
    }

    public Date getDateArrival(){
        return arrival;
    }
//    public void setDeparture(Date departure) {
//        this.departure = departure;
//    }

    public String getArrival() {
        return dateFormat.format(arrival);
    }

//    public void setArrival(Date arrival) {
//        this.arrival = arrival;
//    }

    public Double getTotalMass() {
        return totalMass;
    }

    public void setTotalMass(Double totalMass) {
        this.totalMass = totalMass;
    }

    public Double getDimX() {
        return dimX;
    }

    public void setDimX(Double dimX) {
        this.dimX = dimX;
    }

    public Double getDimY() {
        return dimY;
    }

    public void setDimY(Double dimY) {
        this.dimY = dimY;
    }

    public Double getDimZ() {
        return dimZ;
    }

    public void setDimZ(Double dimZ) {
        this.dimZ = dimZ;
    }

    public Double getPricePerKg() {
        return pricePerKg;
    }

    public void setPricePerKg(Double pricePerKg) {
        this.pricePerKg = pricePerKg;
    }

    public Double getPricePerM2() {
        return pricePerM2;
    }

    public void setPricePerM2(Double pricePerM2) {
        this.pricePerM2 = pricePerM2;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @OrderBy("status")
    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    //Parsers
    public void setDeparture(String departure) {

        DateFormat format = new SimpleDateFormat("d.M.yyyy");
        try {
            this.departure = format.parse(departure);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setArrival(String arrival) {
        DateFormat format = new SimpleDateFormat("d.M.yyyy");
        try {
            this.arrival = format.parse(arrival);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
