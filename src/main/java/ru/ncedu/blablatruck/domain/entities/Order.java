package ru.ncedu.blablatruck.domain.entities;

import ru.ncedu.blablatruck.domain.dao.OrderStatus;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "orders")
public class Order implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String wpFrom;
    private String wpTo;
    private Double dimX;
    private Double dimY;
    private Double mass;
    @Enumerated(EnumType.STRING)
    private OrderStatus status = OrderStatus.STATUS_NEW;

    @ManyToOne
    private Trip trip;

    @ManyToOne
    private User client;

    public Order() {
    }

    public String getWpFrom() {
        return wpFrom;
    }

    public void setWpFrom(String wpFrom) {
        this.wpFrom = wpFrom;
    }

    public String getWpTo() {
        return wpTo;
    }

    public void setWpTo(String wpTo) {
        this.wpTo = wpTo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getDimX() {
        return dimX;
    }

    public void setDimX(Double dimX) {
        this.dimX = dimX;
    }

    public Double getDimY() {
        return dimY;
    }

    public void setDimY(Double dimY) {
        this.dimY = dimY;
    }

    public Double getMass() {
        return mass;
    }

    public void setMass(Double mass) {
        this.mass = mass;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }
}
