package ru.ncedu.blablatruck.domain.entities;

import java.util.Arrays;
import java.util.List;

public enum TripStatus {
    NEW,
    FULL,
    IN_FLIGHT,
    FINISHED,
    CANCELLED;

    public static List<TripStatus> CURRENT = Arrays.asList(NEW, FULL, IN_FLIGHT);
    public static List<TripStatus> COMPLETED = Arrays.asList(FINISHED, CANCELLED);
}