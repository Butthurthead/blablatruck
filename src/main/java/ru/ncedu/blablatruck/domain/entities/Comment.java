package ru.ncedu.blablatruck.domain.entities;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

/**
 * A comment about the user
 */
@Entity
@Table(name = "comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private User author;

    @ManyToOne
    private User user;

    @Type(type = "text")
    @Column(length = 1024)
    private String text;

    private Date receiveDate;

    /**
     * Return user id
     *
     * @return user id
     */
    public int getId() {
        return id;
    }

    /**
     * Set user id
     *
     * @param id user id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Return author of the comment
     *
     * @return author of the comment
     */
    public User getAuthor() {
        return author;
    }

    /**
     * Set author of the comment
     *
     * @param author author of the comment
     */
    public void setAuthor(User author) {
        this.author = author;
    }

    /**
     * Missing JAVADOC
     *
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     * Missing JAVADOC
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Return text of the comment
     *
     * @return text of the comment
     */
    public String getText() {
        return text;
    }

    /**
     * Set text of the comment
     *
     * @param text text of the comment
     */
    public void setText(String text) {
        this.text = text;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }
}
