package ru.ncedu.blablatruck.domain.entities;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Updating constraints requeres update of {@link ru.ncedu.blablatruck.form.forms.RegistrationForm#username}
     * and vice versa
     */
    @Column(unique = true, nullable = false, length = 20)
    private String username;

    private String firstName;

    private String lastName;

    @Column(nullable = false)
    private String password;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(length = 12)
    private String phone;

    private Integer enabled = 1;

    @ManyToOne
    private Role role;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    @OrderBy("receiveDate DESC")
    private List<Comment> receivedComments = new ArrayList<>();

    @OneToMany(mappedBy = "author")
    private List<Comment> createdComments = new ArrayList<>();

    @OneToMany(mappedBy = "client")
    private List<Order> createdOrders = new ArrayList<>();

    @OneToMany(mappedBy = "owner")
    private List<Trip> createdTrips = new ArrayList<>();

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = new BCryptPasswordEncoder().encode(password);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String name) {
        this.username = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Comment> getReceivedComments() {
        return new ArrayList<>(receivedComments);
    }

    public List<Comment> getCreatedComments() {
        return new ArrayList<>(createdComments);
    }

    public void addReceivedComment(Comment comment) {
        receivedComments.add(comment);
    }

    public void addCreatedComment(Comment comment) {
        createdComments.add(comment);
    }

    public List<Order> getCreatedOrders() {
        return createdOrders;
    }

    public void setCreatedOrders(List<Order> createdOrders) {
        this.createdOrders = createdOrders;
    }

    public List<Trip> getCreatedTrips() {
        return createdTrips;
    }

    public void setCreatedTrips(List<Trip> createdTrips) {
        this.createdTrips = createdTrips;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}