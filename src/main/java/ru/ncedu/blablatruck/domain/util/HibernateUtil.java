package ru.ncedu.blablatruck.domain.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.function.Consumer;

public class HibernateUtil {

    public static SessionFactory getSessionFactory() {
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            return (SessionFactory) envContext.lookup("blablatruck/SessionFactory");
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    public static void inTransaction(SessionFactory sessionFactory, Consumer<Session> consumer) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            try {
                transaction = session.beginTransaction();
                consumer.accept(session);
                transaction.commit();
            } catch (Exception e) {
                if (transaction != null) {
                    transaction.rollback();
                }
                throw e;
            }
        }
    }

}
