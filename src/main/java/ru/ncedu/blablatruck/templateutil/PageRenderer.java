package ru.ncedu.blablatruck.templateutil;

import com.google.template.soy.SoyFileSet;
import com.google.template.soy.data.SoyListData;
import com.google.template.soy.data.SoyMapData;
import com.google.template.soy.tofu.SoyTofu;

import java.util.Map;

@Deprecated
public class PageRenderer {
    public static final String DEFAULT_TEMPLATE = "layout.main";
    protected static final String MAIN_MENU_DELEGATE = "anon";

    protected static final SoyTofu tofu = (SoyFileSet.builder()).
            add(PageRenderer.class.getResource("login.soy")).
            add(PageRenderer.class.getResource("profile.soy")).
            add(PageRenderer.class.getResource("layout.soy")).
            add(PageRenderer.class.getResource("forms.soy")).
            add(PageRenderer.class.getResource("registration.soy")).
            add(PageRenderer.class.getResource("mainMenu.soy")).
            build().compileToTofu();

    public static String render(String templateName, Map<String, ?> params) {
        return tofu.newRenderer(templateName).setData(params).render();
    }

    public static String render(String templateName, SoyMapData params) {
        return tofu.newRenderer(templateName).setData(params).render();
    }

    public static SoyMapData createMainMenuData(String title, String contentDelegate, String authUsername) {
        return new SoyMapData(
                "title", title,
                "mainMenuDelegate", MAIN_MENU_DELEGATE,
                "contentDelegate", contentDelegate,
                "menuItems", new SoyListData(
                new SoyMapData(
                        "active", true,
                        "url", "/reg",
                        "name", "Регистрация"
                ),
                (authUsername != null) ?
                        new SoyMapData(
                                "active", true,
                                "url", "/logout",
                                "name", "Выход"
                        )
                        :
                        new SoyMapData(
                                "active", false,
                                "url", "/login",
                                "name", "Вход"
                        )
        ),
                "username", authUsername
        );
    }
}
