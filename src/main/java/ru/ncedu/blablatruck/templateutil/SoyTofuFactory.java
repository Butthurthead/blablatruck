package ru.ncedu.blablatruck.templateutil;

import com.google.template.soy.SoyFileSet;
import com.google.template.soy.tofu.SoyTofu;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.spi.ObjectFactory;
import java.net.URL;
import java.util.Hashtable;

public class SoyTofuFactory implements ObjectFactory {

    @Override
    public SoyTofu getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment)
            throws Exception {
        return SoyFileSet.builder()
                .add(getResource("layout.soy"))
                .add(getResource("forms.soy"))
                .add(getResource("mainMenu.soy"))
                .add(getResource("text.soy"))
                .add(getResource("pages/login.soy"))
                .add(getResource("pages/profile.soy"))
                .add(getResource("pages/registration.soy"))
                .add(getResource("pages/myTrips.soy"))
                .add(getResource("pages/createTrip.soy"))
                .add(getResource("pages/viewTrip.soy"))
                .add(getResource("pages/index.soy"))
                .add(getResource("pages/clientOrders.soy"))
                .add(getResource("pages/findtrip.soy"))
                .add(getResource("parts/driver/order.soy"))
                .add(getResource("parts/driver/trip.soy"))
                .build()
                .compileToTofu();
    }

    private URL getResource(String name) {
        return getClass().getResource(name);
    }
}
