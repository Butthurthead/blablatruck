package ru.ncedu.blablatruck.templateutil;

public class TemplateTags {
    public static final String FORM_DATA = "formData";
    public static final String USERNAME = "username";
    public static final String USER_FULLNAME = "fullname";
    public static final String PHONE = "phone";
    public static final String EMAIL = "email";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String PROFILE_COMMENTS = "comments";
    public static final String PROFILE_COMMENT_AUTHOR = "author";
    public static final String PROFILE_COMMENT_TEXT = "text";
    public static final String PROFILE_COMMENT_RECEIVE_DATE = "receiveDate";
    public static final String TRIPS ="trips";
    public static final String TRIP_ID = "tripId";
    public static final String TRIP_ARRIVAL = "arrival";
    public static final String TRIP_DEPARTURE = "departure";
    public static final String TRIP_DESCRIPTION = "description";
    public static final String TRIP_DIMX = "dimX";
    public static final String TRIP_DIMY = "dimY";
    public static final String TRIP_DIMZ = "dimZ";
    public static final String TRIP_PRICE_PER_KG = "pricePerKG";
    public static final String TRIP_PRICE_PER_M2 = "pricePerM2";
    public static final String TRIP_STATUS = "status";
    public static final String TRIP_TOTAL_MASS = "totalMass";
    public static final String TRIP_DEPARTURE_CITY = "departureCity";
    public static final String TRIP_ARRIVAL_CITY = "arrivalCity";
    public static final String TRIP_LOCATIONS = "locations";
    public static final String TRIP_OWNER_NAME = "owner";
    public static final String TRIP_POINTS = "points";

    public static final String ORDERS = "orders";
    public static final String ORDER_ID = "orderId";
    public static final String ORDER_FROM = "orderFrom";
    public static final String ORDER_TO = "orderTo";
    public static final String ORDER_DIMX = "orderDimX";
    public static final String ORDER_DIMY = "orderDimY";
    public static final String ORDER_MASS = "orderMass";
    public static final String ORDER_ID_FOR_DELETE = "orderIdForDelete";
    public static final String ORDER_CAN_DELETE = "canDelete";
    public static final String ORDER_STATUS = "status";

    public static final String LAST_TRIP_ID = "lastTripId";
    public static final String NEED_MORE = "needMore";
    public static final String VIA_POINTS = "viaPoint";
    public static final String ACTION = "action";
    public static final String ID = "id";
    public static final String LOCATION_NAME = "lname";
    public static final int M_SUCCESS_REG = 0;

    public static class FormTags {
        public static final String FAILED = "failed";
        public static final String ACTION = "action";
        public static final String MESSAGE = "message";
        public static final String SUBMIT_LABEL = "submitLabel";
        public static final String FIELDS = "fields";

        public static final String NAME = "name";
        public static final String VALUE = "value";
        public static final String VALUES = "values";
        public static final String LABEL = "label";
        public static final String TYPE = "type";
        public static final String OPTIONS = "options";
        public static final String OPTION_LABEL = "label";
        public static final String OPTION_VALUE = "value";
        public static final String ERROR = "error";
    }

    public static class LabelTags {
        public static final String ERR_EMPTY = "errEmpty";
        public static final String ERR_NEGATIVE = "errNegative";
        public static final String ERR_USER_EXISTS = "errUserExists";
        public static final String ERR_TODAY_OR_LATER = "errTodayOrLater";
        public static final String ERR_INCORRECT_EMAIL = "errIcorrectEmail";
        public static final String ERR_INCORRECT_PHONE = "errIncorrectPhone";
        public static final String ERR_CONFIRM_PASSWORD = "errConfirmPassword";
        public static final String ERR_EMAIL_EXISTS = "errEmailExists";
        public static final String ERR_INCORRECT_PASSWORD = "errIncorrectPassword";
        public static final String ERR_TRIP_DATES = "errTripDates";

        public static final String LBL_COMMENT = "lblComment";
        public static final String LBL_WIDTH = "lblWidth";
        public static final String LBL_FROM = "lblFrom";
        public static final String LBL_TO = "lblTo";
        public static final String LBL_LENGTH = "lblLength";
        public static final String LBL_HEIGHT = "lblHeight";
        public static final String LBL_V = "lblV";
        public static final String LBL_MASS = "lblMass";
        public static final String LBL_LOGIN = "lblLogin";
        public static final String LBL_DEPARTURE_CITY = "lblDepartureCity";
        public static final String LBL_ARRIVAL_CITY = "lblArrivalCity";
        public static final String LBL_DESCRIPTION = "lblDescription";
        public static final String LBL_DEPARTURE_DATE = "lblDepartureDate";
        public static final String LBL_ARRIVAL_DATE = "lblArrivalDate";
        public static final String LBL_TOTAL_MASS = "lblTotalMass";
        public static final String LBL_MIN_TOTAL_MASS = "lblMinTotalMass";
        public static final String LBL_TOTAL_V = "lblTotalV";
        public static final String LBL_MAX_WIDTH = "lblMaxWidth";
        public static final String LBL_MAX_LENGTH = "lblMaxLength";
        public static final String LBL_MAX_HEIGHT = "lblMaxHeight";
        public static final String LBL_PRICE_PER_KG = "lblPricePerKg";
        public static final String LBL_PRICE_PER_M2 = "lblPricePerM2";
        public static final String LBL_FIRST_NAME = "lblFirstName";
        public static final String LBL_LAST_NAME = "lblLastName";
        public static final String LBL_PHONE = "lblPhone";
        public static final String LBL_EMAIL = "lblEmail";
        public static final String LBL_PASSWORD = "lblPassword";
        public static final String LBL_CONFIRM_PASSWORD = "lblConfirmPassword";
        public static final String LBL_COMMIT = "lblCommit";
        public static final String LBL_REJECT = "lblReject";
        public static final String LBL_SHIP = "lblShip";
        public static final String LBL_DELIVER = "lblDeliver";

        public static final String MSG_FAILED = "msgFailed";
        public static final String MSG_UPDATE_SUCCESS = "msgUpdateSuccess";

        public static final String SBM_DEFAULT = "sbmDefault";
        public static final String SBM_SAVE = "sbmSave";
        public static final String SBM_POST_COMMENT = "sbmPostComment";
        public static final String SBM_SET_FILTER = "sbmSetFilter";
    }
}
