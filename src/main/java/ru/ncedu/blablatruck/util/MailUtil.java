package ru.ncedu.blablatruck.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service("mailService")
public class MailUtil {
    private static final String SIGNATURE = "\n\nС уважением,\nАдминистрация портала BlaBlaTruck.local\nblablatruck@bk.ru";

    @Autowired
    private MailSender mailSender;

    @Autowired
    private SimpleMailMessage preConfiguredMessage;

    /**
     * This method will send compose and send the message
     * @param type : type of message: 0 - success registration
     */
    public void sendMail(String to, String login, int type) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        switch (type){
            case 0:
                message.setSubject("Регистрация в проекте BlaBlaTruck");
                message.setText(registrationMail(login));
                break;
            default:
                break;
        }
        mailSender.send(message);
    }

    /**
     * This method will send a pre-configured message
     */
    public void sendPreConfiguredMail(String message) {
        SimpleMailMessage mailMessage = new SimpleMailMessage(preConfiguredMessage);
        mailMessage.setText(message);
        mailSender.send(mailMessage);
    }

    private String registrationMail(String login){
        return "Поздравляем!\n" +
                "Вы успешно зарегистрировались на серивсе BlaBlaTruck.\n" +
                "Ваш логин для входа: "+login+"\n\n " +
                "Желаем приятных поездок и быстрых перевозок!\n" + SIGNATURE;
    }
}
