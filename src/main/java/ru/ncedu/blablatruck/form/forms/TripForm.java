package ru.ncedu.blablatruck.form.forms;

import org.hibernate.validator.constraints.NotBlank;
import ru.ncedu.blablatruck.form.annotation.constraint.ChronologicalDates;
import ru.ncedu.blablatruck.form.annotation.constraint.TodayOrLater;
import ru.ncedu.blablatruck.form.converters.RequestParameterValueConverter;
import ru.ncedu.blablatruck.form.annotation.Form;
import ru.ncedu.blablatruck.form.annotation.FormField;
import static ru.ncedu.blablatruck.templateutil.TemplateTags.LabelTags.*;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.util.Date;

@ChronologicalDates(dateFields = {"departure", "arrival"},
        message = ERR_TRIP_DATES)
@Form(action = "/createtrip", submitLabel = SBM_SAVE)
public class TripForm{

    @NotBlank(message = ERR_EMPTY)
    @FormField(formFieldName = "departureCity",
            entityFieldName = "departureCity",
            formFieldLabel = LBL_DEPARTURE_CITY,
            valueConverter = RequestParameterValueConverter.class)
    private String departureCity;


    @NotBlank(message = ERR_EMPTY)
    @FormField(formFieldName = "arrivalCity",
            entityFieldName = "arrivalCity",
            formFieldLabel = LBL_ARRIVAL_CITY,
            valueConverter = RequestParameterValueConverter.class)
    private String arrivalCity;

    @FormField(formFieldName = "description",
            entityFieldName = "description",
            formFieldLabel = LBL_DESCRIPTION,
            valueConverter = RequestParameterValueConverter.class)
    private String description;

    @NotNull(message = ERR_EMPTY)
    @TodayOrLater(message = ERR_TODAY_OR_LATER)
    @FormField(formFieldName = "departure",
            entityFieldName = "departure",
            formFieldType = "date",
            formFieldLabel = LBL_DEPARTURE_DATE,
            extractiveType = "java.lang.String",
            valueConverter = RequestParameterValueConverter.class)
    private Date departure;

    @NotNull(message = ERR_EMPTY)
    @FormField(formFieldName = "arrival",
            entityFieldName = "arrival",
            formFieldType = "date",
            formFieldLabel = LBL_ARRIVAL_DATE,
            extractiveType = "java.lang.String",
            valueConverter = RequestParameterValueConverter.class)
    private Date arrival;

    @NotNull(message = ERR_EMPTY)
    @DecimalMin(value = "0.00", message = ERR_NEGATIVE)
    @FormField(formFieldName = "totalMass",
            entityFieldName = "totalMass",
            formFieldLabel = LBL_TOTAL_MASS,
            formFieldType = "float",
            valueConverter = RequestParameterValueConverter.class)
    private Double totalMass;

    @NotNull(message = ERR_EMPTY)
    @DecimalMin(value = "0.00", message = ERR_NEGATIVE)
    @FormField(formFieldName = "dimX",
            entityFieldName = "dimX",
            formFieldLabel = LBL_MAX_LENGTH,
            formFieldType = "float",
            valueConverter = RequestParameterValueConverter.class)
    private Double dimX;

    @NotNull(message = ERR_EMPTY)
    @DecimalMin(value = "0.00", message = ERR_NEGATIVE)
    @FormField(formFieldName = "dimY",
            entityFieldName = "dimY",
            formFieldLabel = LBL_MAX_HEIGHT,
            formFieldType = "float",
            valueConverter = RequestParameterValueConverter.class)
    private Double dimY;

    @NotNull(message = ERR_EMPTY)
    @DecimalMin(value = "0.00", message = ERR_NEGATIVE)
    @FormField(formFieldName = "dimZ",
            entityFieldName = "dimZ",
            formFieldLabel = LBL_MAX_WIDTH,
            formFieldType = "float",
            valueConverter = RequestParameterValueConverter.class)
    private Double dimZ;

    @NotNull(message = ERR_EMPTY)
    @DecimalMin(value = "0.00", message = ERR_NEGATIVE)
    @FormField(formFieldName = "pricePerKg",
            entityFieldName = "pricePerKg",
            formFieldLabel = LBL_PRICE_PER_KG,
            formFieldType = "float",
            valueConverter = RequestParameterValueConverter.class)
    private Double pricePerKg;

    @NotNull(message = ERR_EMPTY)
    @DecimalMin(value = "0.00", message = ERR_NEGATIVE)
    @FormField(formFieldName = "pricePerM2",
            entityFieldName = "pricePerM2",
            formFieldLabel = LBL_PRICE_PER_M2,
            formFieldType = "float",
            valueConverter = RequestParameterValueConverter.class)
    private Double pricePerM2;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public Double getTotalMass() {
        return totalMass;
    }

    public void setTotalMass(Double totalMass) {
        this.totalMass = totalMass;
    }

    public Double getDimX() {
        return dimX;
    }

    public void setDimX(Double dimX) {
        this.dimX = dimX;
    }

    public Double getDimY() {
        return dimY;
    }

    public void setDimY(Double dimY) {
        this.dimY = dimY;
    }

    public Double getDimZ() {
        return dimZ;
    }

    public void setDimZ(Double dimZ) {
        this.dimZ = dimZ;
    }

    public Double getPricePerKg() {
        return pricePerKg;
    }

    public void setPricePerKg(Double pricePerKg) {
        this.pricePerKg = pricePerKg;
    }

    public Double getPricePerM2() {
        return pricePerM2;
    }

    public void setPricePerM2(Double pricePerM2) {
        this.pricePerM2 = pricePerM2;
    }

    public String getDepartureCity() { return departureCity; }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

}
