package ru.ncedu.blablatruck.form.forms;

import org.hibernate.validator.constraints.NotBlank;
import ru.ncedu.blablatruck.form.annotation.Form;
import ru.ncedu.blablatruck.form.annotation.FormField;
import static ru.ncedu.blablatruck.templateutil.TemplateTags.LabelTags.*;

@Form(action = "/postcomment", submitLabel = SBM_POST_COMMENT)
public class CommentForm {

    @NotBlank
    @FormField(formFieldName="receiver", formFieldType = "hidden")
    private String receiver;

    @NotBlank(message = ERR_EMPTY)
    @FormField(formFieldName = "text", entityFieldName = "text", formFieldType = "textarea",
            formFieldLabel = LBL_COMMENT)
    private String text;

    public String getReceiver() {return receiver;}

    public void setReceiver(String receiver) {this.receiver = receiver;}

    public String getText() {return text;}

    public void setText(String text) {this.text = text;}

}
