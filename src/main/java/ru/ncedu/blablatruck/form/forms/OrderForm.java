package ru.ncedu.blablatruck.form.forms;

import ru.ncedu.blablatruck.form.annotation.Form;
import ru.ncedu.blablatruck.form.annotation.FormField;
import static ru.ncedu.blablatruck.templateutil.TemplateTags.LabelTags.*;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

@Form(action = "/viewtrip")
public class OrderForm {

    @FormField(formFieldName = "tripId", formFieldType = "hidden")
    private Integer tripId;

    @NotNull(message = ERR_EMPTY)
    @FormField(formFieldName = "wpFrom", entityFieldName= "wpFrom", formFieldLabel = LBL_FROM, formFieldType = "string")
    private String wpFrom;

    @NotNull(message = ERR_EMPTY)
    @FormField(formFieldName = "wpTo", entityFieldName= "wpTo", formFieldLabel = LBL_TO, formFieldType = "string")
    private String wpTo;

    @NotNull(message = ERR_EMPTY)
    @DecimalMin(value = "0.00", message = ERR_NEGATIVE)
    @FormField(formFieldName = "dimX", entityFieldName= "dimX", formFieldLabel = LBL_WIDTH, formFieldType = "float")
    private Double dimX;

    @NotNull(message = ERR_EMPTY)
    @DecimalMin(value = "0.00", message = ERR_NEGATIVE)
    @FormField(formFieldName = "dimY", entityFieldName= "dimY", formFieldLabel = LBL_LENGTH, formFieldType = "float")
    private Double dimY;

    @NotNull(message = ERR_EMPTY)
    @DecimalMin(value = "0.00", message = ERR_NEGATIVE)
    @FormField(formFieldName = "mass", entityFieldName= "mass", formFieldLabel = LBL_MASS, formFieldType = "float")
    private Double mass;

    public Integer getTripId() {
        return tripId;
    }

    public Double getDimX() {
        return dimX;
    }

    public Double getDimY() {
        return dimY;
    }

    public Double getMass() {
        return mass;
    }

    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    public void setDimX(Double dimX) {
        this.dimX = dimX;
    }

    public void setDimY(Double dimY) {
        this.dimY = dimY;
    }

    public void setMass(Double mass) {
        this.mass = mass;
    }

    public String getWpFrom() {
        return wpFrom;
    }

    public void setWpFrom(String wpFrom) {
        this.wpFrom = wpFrom;
    }

    public String getWpTo() {
        return wpTo;
    }

    public void setWpTo(String wpTo) {
        this.wpTo = wpTo;
    }
}
