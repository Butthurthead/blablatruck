package ru.ncedu.blablatruck.form.forms;

import org.hibernate.validator.constraints.NotBlank;
import ru.ncedu.blablatruck.domain.entities.User;
import ru.ncedu.blablatruck.form.annotation.Form;
import ru.ncedu.blablatruck.form.annotation.FormField;
import ru.ncedu.blablatruck.form.annotation.constraint.UniqueUsername;
import static ru.ncedu.blablatruck.templateutil.TemplateTags.LabelTags.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Form(action="/registration")
public class RegistrationForm extends UserCommon {

    /**
     * Updating constraints requeres update of {@link User#username}
     * and vice versa.
     */
    @UniqueUsername(message = ERR_USER_EXISTS)
    @NotNull
    @Size(min = 5, max = 20)
    @FormField(formFieldName = "username",
            entityFieldName = "username",
            formFieldLabel = LBL_LOGIN)
    private String username;


    // NotBlank is needed for additional constraint on password presence. There is no @NotBlank
    // constraint in UserCommonn, because ProfileForm password field can be empty.
    // Overriding password field is not a good idea, so getter for password field is annotated,
    // validation result will be the same.
    @NotBlank(message = ERR_EMPTY)
    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }
}
