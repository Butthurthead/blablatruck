package ru.ncedu.blablatruck.form.forms;

import ru.ncedu.blablatruck.form.annotation.Form;
import static ru.ncedu.blablatruck.templateutil.TemplateTags.LabelTags.*;

@Form(action = "/profile",
        failMessage = MSG_FAILED,
        successMessage = MSG_UPDATE_SUCCESS,
        submitLabel = SBM_SAVE)
public class ProfileForm extends UserCommon {}
