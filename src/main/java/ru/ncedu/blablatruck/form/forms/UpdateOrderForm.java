package ru.ncedu.blablatruck.form.forms;

import ru.ncedu.blablatruck.domain.dao.OrderDAO;
import ru.ncedu.blablatruck.domain.dao.OrderStatus;
import ru.ncedu.blablatruck.domain.entities.Order;
import ru.ncedu.blablatruck.form.annotation.Form;
import ru.ncedu.blablatruck.form.annotation.FormField;
import ru.ncedu.blablatruck.form.annotation.FormFieldOption;
import static ru.ncedu.blablatruck.templateutil.TemplateTags.LabelTags.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Form(action = "/orders")
public class UpdateOrderForm {

    @FormFieldOption
    static class StatusOption {
        private String value;
        private String label;

        public StatusOption(OrderStatus status) {
            this.value = status.toString();
            switch (status) {
                case STATUS_COMMITTED:
                    label = LBL_COMMIT;
                    break;
                case STATUS_REJECTED:
                    label = LBL_REJECT;
                    break;
                case STATUS_SHIPPED:
                    label = LBL_SHIP;
                    break;
                case STATUS_DELIVERED:
                    label = LBL_DELIVER;
                    break;
                default:
                    label = "";
            }
        }
    }

    @NotNull
    @FormField(formFieldName = "orderId",
            entityFieldName = "id",
            formFieldType = "hidden")
    private Integer orderId;

    @FormField(formFieldName = "status",
            entityFieldName = "status",
            formFieldType = "buttons",
            formFieldOptions = "statusOptions")
    private String status;

    private List<StatusOption> statusOptions = new ArrayList<>();

    public void initStatusOptions(Order order, String username) {
        for (OrderStatus status : OrderStatus.values()) {
            try {
                if (!status.equals(order.getStatus())) {
                    OrderDAO.permitStatusUpdating(order, username, status);
                    statusOptions.add(new StatusOption(status));
                }
            } catch (UnsupportedOperationException | SecurityException ignored) { }
        }
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
