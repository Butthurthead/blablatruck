package ru.ncedu.blablatruck.form.forms;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import ru.ncedu.blablatruck.form.annotation.constraint.EqualFieldValues;
import ru.ncedu.blablatruck.form.annotation.FormField;
import ru.ncedu.blablatruck.form.annotation.constraint.UniqueEmail;
import ru.ncedu.blablatruck.form.forms.groups.UniqueEmailGroup;

import static ru.ncedu.blablatruck.templateutil.TemplateTags.LabelTags.*;

import javax.validation.constraints.Pattern;

@EqualFieldValues(fields = {"password", "confirmPassword"},
        message = ERR_CONFIRM_PASSWORD,
        errorField = "confirmPassword")
public abstract class UserCommon {
    @NotBlank(message = ERR_EMPTY)
    @FormField(formFieldName = "firstName",
            entityFieldName = "firstName",
            formFieldLabel = LBL_FIRST_NAME)
    private String firstName;

    @NotBlank(message = ERR_EMPTY)
    @FormField(formFieldName = "lastName",
            entityFieldName = "lastName",
            formFieldLabel = LBL_LAST_NAME)
    private String lastName;

    @Pattern(regexp = "^(8)?[0-9]{10}$", message = ERR_INCORRECT_PHONE)
    @FormField(formFieldName = "phone",
            entityFieldName = "phone",
            formFieldLabel = LBL_PHONE)
    private String phone;

    @NotBlank(message = ERR_EMPTY)
    @Email(message = ERR_INCORRECT_EMAIL)
    @UniqueEmail(message = ERR_EMAIL_EXISTS, groups = {UniqueEmailGroup.class})
    @FormField(formFieldName = "email",
            entityFieldName = "email",
            formFieldLabel = LBL_EMAIL,
            formFieldType = "email")
    private String email;

    @Pattern(regexp = "^|([a-zA-Z0-9]{6,50})$",
            message = ERR_INCORRECT_PASSWORD)
    @FormField(formFieldName = "password",
            formFieldLabel = LBL_PASSWORD,
            formFieldType = "password")
    protected String password;

    @FormField(formFieldName = "confirmPassword",
            formFieldLabel = LBL_CONFIRM_PASSWORD,
            formFieldType = "password")
    private String confirmPassword;

    public String getFirstName() {return firstName;}

    public void setFirstName(String firstName) {this.firstName = firstName;}

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) {this.lastName = lastName;}

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
