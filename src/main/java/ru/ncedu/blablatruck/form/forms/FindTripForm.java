package ru.ncedu.blablatruck.form.forms;

import org.hibernate.validator.constraints.NotBlank;
import ru.ncedu.blablatruck.form.annotation.Form;
import ru.ncedu.blablatruck.form.annotation.FormField;
import ru.ncedu.blablatruck.form.converters.RequestParameterValueConverter;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import static ru.ncedu.blablatruck.templateutil.TemplateTags.LabelTags.*;
import static ru.ncedu.blablatruck.templateutil.TemplateTags.LabelTags.LBL_TOTAL_MASS;
@Form(action="/findtrip", submitLabel = SBM_SET_FILTER)
public class FindTripForm {
    @FormField(formFieldName = "departureCity",
            entityFieldName = "departureCity",
            formFieldLabel = LBL_DEPARTURE_CITY,
            valueConverter = RequestParameterValueConverter.class)
    private String departureCity;


    @FormField(formFieldName = "arrivalCity",
            entityFieldName = "arrivalCity",
            formFieldLabel = LBL_ARRIVAL_CITY,
            valueConverter = RequestParameterValueConverter.class)
    private String arrivalCity;

    @DecimalMin(value = "0.00", message = ERR_NEGATIVE)
    @FormField(formFieldName = "totalMass",
            entityFieldName = "totalMass",
            formFieldLabel = LBL_MIN_TOTAL_MASS,
            formFieldType = "float",
            valueConverter = RequestParameterValueConverter.class)
    private Double totalMass;

    @DecimalMin(value = "0.00", message = ERR_NEGATIVE)
    @FormField(formFieldName = "totalV",
            entityFieldName = "totalV",
            formFieldLabel = LBL_TOTAL_V,
            formFieldType = "float",
            valueConverter = RequestParameterValueConverter.class)
    private Double totalV;

    public Double getTotalV() {
        return totalV;
    }

    public void setTotalV(Double totalV) {
        this.totalV = totalV;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Double getTotalMass() {
        return totalMass;
    }

    public void setTotalMass(Double totalMass) {
        this.totalMass = totalMass;
    }
}
