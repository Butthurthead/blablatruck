package ru.ncedu.blablatruck.form.util;

import com.google.template.soy.data.SoyListData;
import com.google.template.soy.data.SoyMapData;
import ru.ncedu.blablatruck.form.annotation.FormField;
import ru.ncedu.blablatruck.form.converters.ValueConverter;

import javax.validation.ConstraintViolation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class FormUtil {

    public static Field findField(Class<?> clazz, String fieldName) throws NoSuchFieldException {
        for (Class<?> cls = clazz; cls != null; cls = cls.getSuperclass()) {
            try {
                return cls.getDeclaredField(fieldName);
            } catch (NoSuchFieldException ignored) {}
        }
        throw new NoSuchFieldException();
    }

    public static List<Field> getAllFields(Class<?> clazz) {
        List<Field> fields = new ArrayList<>();
        for (Class cls = clazz; cls != null; cls = cls.getSuperclass()) {
            fields.addAll(Arrays.asList(cls.getDeclaredFields()));
        }
        return fields;
    }

    @Deprecated
    public static SoyMapData extractSoyMap(Object form) throws IllegalAccessException, InstantiationException {
        SoyMapData data = new SoyMapData();
        Field[] fields = form.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(FormField.class)) {
                FormField annotation = field.getAnnotation(FormField.class);
                String fieldName = annotation.formFieldName();
                Object value = field.get(form);

                // IMPORTANT: convert value to String here

                if (value != null) {
                    data.put(fieldName, value);
                }

            }
        }
        return data;
    }


    @Deprecated
    public static <T> SoyListData collectErrors(Set<ConstraintViolation<T>> violations) {
        SoyListData errors = new SoyListData();
        for (ConstraintViolation<T> violation : violations) {
            errors.add(violation.getPropertyPath().toString() + " " + violation.getMessage());
        }
        return errors;
    }

    public static void updateEntity(Object entity, Object form)
            throws IllegalAccessException, InstantiationException, NoSuchFieldException {
        List<Field> fields = getAllFields(form.getClass());
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(FormField.class)) {
                FormField annotation = field.getAnnotation(FormField.class);
                String fieldName = annotation.entityFieldName();
                if (!fieldName.equals("")) {
                    Field entityField = entity.getClass().getDeclaredField(fieldName);
                    entityField.setAccessible(true);
                    // IMPORTANT: convert value here
                    Object value = annotation.valueConverter().newInstance()
                            .convert(field.get(form), entityField.getType());
                    entityField.set(entity, value);
                }
            }
        }
    }
}
