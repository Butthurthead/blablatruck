package ru.ncedu.blablatruck.form.converters;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * General "all-purpose" value converter. Use at your own risk.
 *
 * The current implementation is very poor and supports the following transformations:
 * 1. T -> T
 * 2. String -> int
 */
public class DefaultValueConverter implements ValueConverter {

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * Converts the object from one type to another and throws tons of exceptions for sure.
     *
     * @param value      the object to convert
     * @param targetType the target type
     * @return an object of the target type
     */
    @Override
    public Object convert(Object value, Type targetType) {
        if (value == null) {
            return getDefaultValue(targetType);
        }

        Class sourceType = value.getClass();
        if (sourceType == targetType) {
            return value;
        }
        try {
            if (targetType == String.class) {
                if (sourceType == Date.class)
                    return new SimpleDateFormat(DATE_FORMAT).format((Date)value);
                return value.toString();
            }
            if (sourceType == String.class && (targetType == int.class || targetType == Integer.class)) {
                return Integer.parseInt((String) value);
            }
            if (sourceType == String.class && (targetType == double.class || targetType == Double.class)) {
                return Double.parseDouble((String) value);
            }
            if (sourceType == String.class && (targetType == Date.class)) {
                return new SimpleDateFormat(DATE_FORMAT).parse((String)value);
            }
        } catch (NumberFormatException | ParseException ignored) {}

        return null;
    }

    private Object getDefaultValue(Type targetType) {
        if (targetType == null) {
            throw new IllegalArgumentException();
        }
        if (targetType == int.class || targetType == double.class) {
            return 0;
        }
        if (targetType == String.class) {
            return null;
        }
        return null;
    }
}
