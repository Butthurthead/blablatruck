package ru.ncedu.blablatruck.form.validators;

import ru.ncedu.blablatruck.form.annotation.constraint.ChronologicalDates;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import java.util.Date;

public class ChronologicalDatesValidator implements ConstraintValidator<ChronologicalDates, Object> {
    private String[] dateFields;

    @Override
    public void initialize(ChronologicalDates chronologicalDates) {
        dateFields = chronologicalDates.dateFields();
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext constraintValidatorContext) {
        Class<?> cls = object.getClass();
        Date prevDate = null;
        for (String dateFieldName : dateFields) {
            try {
                Field field =  cls.getDeclaredField(dateFieldName);
                field.setAccessible(true);
                if (field.getType().equals(Date.class)) {
                    Date curDate = (Date)field.get(object);
                    if (curDate != null) {
                        if ( prevDate != null && !(curDate.after(prevDate) || curDate.equals(prevDate)) ) {
                            addErrorMessage(constraintValidatorContext, dateFieldName,
                                    constraintValidatorContext.getDefaultConstraintMessageTemplate());
                            return false;
                        }
                        prevDate = curDate;
                    }
                }
            } catch (ReflectiveOperationException refl) {
                refl.printStackTrace(System.out);
                addErrorMessage(constraintValidatorContext, dateFieldName, "Ошибка при обработке данных");
                return false;
            }
        }
        return true;
    }

    private void addErrorMessage(ConstraintValidatorContext constraintValidatorContext, String errorFieldName,
                                 String template) {
        constraintValidatorContext.disableDefaultConstraintViolation();
        constraintValidatorContext.buildConstraintViolationWithTemplate(template).addPropertyNode(errorFieldName).addConstraintViolation();
    }

}
