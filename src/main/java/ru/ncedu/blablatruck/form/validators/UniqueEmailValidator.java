package ru.ncedu.blablatruck.form.validators;


import ru.ncedu.blablatruck.domain.dao.UserDAO;
import ru.ncedu.blablatruck.domain.dao.implementations.HibernateUserDAO;
import ru.ncedu.blablatruck.form.annotation.constraint.UniqueEmail;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String> {
    @Override
    public void initialize(UniqueEmail uniqueEmail) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        UserDAO userDAO = new HibernateUserDAO();
        return userDAO.isEmailUnique(s);
    }
}
