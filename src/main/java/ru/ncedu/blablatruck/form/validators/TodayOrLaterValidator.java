package ru.ncedu.blablatruck.form.validators;

import ru.ncedu.blablatruck.form.annotation.constraint.TodayOrLater;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Calendar;
import java.util.Date;

public class TodayOrLaterValidator implements ConstraintValidator<TodayOrLater, Date> {

    @Override
    public void initialize(TodayOrLater todayOrLater) {}

    @Override
    public boolean isValid(Date date, ConstraintValidatorContext constraintValidatorContext) {
        if (date != null) {
            Calendar now = Calendar.getInstance();
            Calendar toCheck = Calendar.getInstance();
            toCheck.setTime(date);
            return (toCheck.after(now) || (
                        toCheck.get(Calendar.YEAR) == now.get(Calendar.YEAR) &&
                        toCheck.get(Calendar.MONTH) == now.get(Calendar.MONTH) &&
                        toCheck.get(Calendar.DAY_OF_MONTH) == now.get(Calendar.DAY_OF_MONTH)
                    ));
        }
        return true;
    }

}
