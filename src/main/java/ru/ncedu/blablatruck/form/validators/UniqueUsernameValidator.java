package ru.ncedu.blablatruck.form.validators;

import ru.ncedu.blablatruck.domain.dao.UserDAO;
import ru.ncedu.blablatruck.domain.dao.implementations.HibernateUserDAO;
import ru.ncedu.blablatruck.form.annotation.constraint.UniqueUsername;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, String> {

    @Override
    public void initialize(UniqueUsername uniqueUsername) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        UserDAO userDAO = new HibernateUserDAO();
        return userDAO.isUsernameUnique(s);
    }
}
