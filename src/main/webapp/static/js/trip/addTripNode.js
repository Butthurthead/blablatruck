// This file was automatically generated from file.soy.
// Please don't edit this file by hand.

/**
 * @fileoverview Templates in namespace findTrips.
 */

if (typeof findTrips == 'undefined') { var findTrips = {}; }


findTrips.tripNode = function(opt_data, opt_ignored) {
  opt_data = opt_data || {};
  return '<div class="panel panel-success"><div class="panel-heading" style="background-color: #dff0d8;" onMouseOver="this.style.backgroundColor=\'#efefef\';" onMouseOut="this.style.backgroundColor=\'#dff0d8\'" onclick="location.href=\'/viewtrip?tripId=' + soy.$$escapeHtml(opt_data.trip.tripId) + '\'"><h2 class="panel-title">' + soy.$$escapeHtml(opt_data.trip.departure) + ' ' + soy.$$escapeHtml(opt_data.trip.departureCity) + ' &mdash; ' + soy.$$escapeHtml(opt_data.trip.arrivalCity) + '<a href = "/viewtrip?tripId=' + soy.$$escapeHtml(opt_data.trip.tripId) + '" class="panel-title-ext">\u043F\u043E\u0434\u0440\u043E\u0431\u043D\u0435\u0435</a></h2></div><div class="panel-body"><div class="row"><div class="col-md-3"><strong>\u041E\u043F\u0438\u0441\u0430\u043D\u0438\u0435</strong></div><div class="col-md-9">' + soy.$$escapeHtml(opt_data.trip.description) + '</div></div><div class="row"><div class="col-md-3"><strong>\u0421\u0442\u0430\u0442\u0443\u0441</strong></div><div class="col-md-9">' + soy.$$escapeHtml(opt_data.trip.status) + '</div></div><div class="row"><div class="col-md-3"><strong>\u0414\u0430\u0442\u0430 \u043E\u0442\u043F\u0440\u0430\u0432\u043B\u0435\u043D\u0438\u044F</strong></div><div class="col-md-3">' + soy.$$escapeHtml(opt_data.trip.departure) + '</div><div class="col-md-3"><strong>\u0414\u0430\u0442\u0430 \u043F\u0440\u0438\u0431\u044B\u0442\u0438\u044F</strong></div><div class="col-md-3">' + soy.$$escapeHtml(opt_data.trip.arrival) + '</div></div><div class="row"><div class="col-md-3"><strong>\u0413\u043E\u0440\u043E\u0434 \u043E\u0442\u043F\u0440\u0430\u0432\u043B\u0435\u043D\u0438\u044F</strong></div><div class="col-md-3">' + soy.$$escapeHtml(opt_data.trip.departureCity) + '</div><div class="col-md-3"><strong>\u0413\u043E\u0440\u043E\u0434 \u043F\u0440\u0438\u0431\u044B\u0442\u0438\u044F</strong></div><div class="col-md-3">' + soy.$$escapeHtml(opt_data.trip.arrivalCity) + '</div></div><div class="row"><div class="col-md-3"><strong>\u0414\u043E\u043F\u0443\u0441\u0442\u0438\u043C\u0430\u044F \u043C\u0430\u0441\u0441\u0430, \u043A\u0433</strong></div><div class="col-md-3">' + soy.$$escapeHtml(opt_data.trip.totalMass) + '</div><div class="col-md-3"><strong>\u0414\u043E\u043F\u0443\u0441\u0442\u0438\u043C\u044B\u0439 \u043E\u0431\u044A\u0435\u043C, \u0441\u043C</strong></div><div class="col-md-3">' + soy.$$escapeHtml(opt_data.trip.dimX) + 'x' + soy.$$escapeHtml(opt_data.trip.dimY) + 'x' + soy.$$escapeHtml(opt_data.trip.dimZ) + '</div></div><div class="row"><div class="col-md-3"><strong>\u0426\u0435\u043D\u0430 \u0437\u0430 \u043A\u0438\u043B\u043E\u0433\u0440\u0430\u043C\u043C</strong></div><div class="col-md-3">' + soy.$$escapeHtml(opt_data.trip.pricePerKG) + '</div><div class="col-md-3"><strong>\u0426\u0435\u043D\u0430 \u0437\u0430 \u043C<sup>3</sup></strong></div><div class="col-md-3">' + soy.$$escapeHtml(opt_data.trip.pricePerM2) + '</div></div></div></div>';
};
if (goog.DEBUG) {
  findTrips.tripNode.soyTemplateName = 'findTrips.tripNode';
}
