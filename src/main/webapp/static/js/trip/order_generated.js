// This file was automatically generated from order.soy.
// Please don't edit this file by hand.

/**
 * @fileoverview Templates in namespace driver.order.
 */

if (typeof driver == 'undefined') { var driver = {}; }
if (typeof driver.order == 'undefined') { driver.order = {}; }


driver.order.status = function(opt_data, opt_ignored, opt_ijData) {
  var output = '';
  switch (opt_data.status) {
    case 'STATUS_NEW':
      output += '\u041D\u043E\u0432\u044B\u0439';
      break;
    case 'STATUS_COMMITTED':
      output += '\u041F\u043E\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043D\u043D\u044B\u0439';
      break;
    case 'STATUS_SHIPPED':
      output += '\u041E\u0442\u0433\u0440\u0443\u0436\u0435\u043D\u043D\u044B\u0439';
      break;
    case 'STATUS_DELIVERED':
      output += '\u0414\u043E\u0441\u0442\u0430\u0432\u043B\u0435\u043D\u043D\u044B\u0439';
      break;
    case 'STATUS_REJECTED':
      output += '\u041E\u0442\u043A\u043B\u043E\u043D\u0435\u043D\u043D\u044B\u0439';
      break;
    default:
      output += '\u041D\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043D\u044B\u0439';
  }
  return output;
};
if (goog.DEBUG) {
  driver.order.status.soyTemplateName = 'driver.order.status';
}


driver.order.buttons = function(opt_data, opt_ignored, opt_ijData) {
  var output = '';
  switch (opt_data.status) {
    case 'STATUS_NEW':
      output += '<div class="btn-group btn-group-xs"><form class="btn-group btn-group-xs" method="POST" name="accept' + soy.$$escapeHtml(opt_data.id) + '"><input type="hidden" name="action" value="update_order_status"/><input type="hidden" name="order_id" value="' + soy.$$escapeHtml(opt_data.id) + '"/><input type="hidden" name="new_status" value="STATUS_COMMITTED"/><input type="hidden"  name="' + soy.$$escapeHtml(opt_ijData.csrfName) + '" value="' + soy.$$escapeHtml(opt_ijData.csrfValue) + '"/><button type="submit" class="btn btn-info" onclick="changeStatus(event, \'accept\', ' + soy.$$escapeHtml(opt_data.id) + ');">\u041F\u0440\u0438\u043D\u044F\u0442\u044C</button></form><form class="btn-group btn-group-xs" method="POST" name="refuse' + soy.$$escapeHtml(opt_data.id) + '"><input type="hidden" name="action" value="update_order_status"/><input type="hidden" name="order_id" value="' + soy.$$escapeHtml(opt_data.id) + '"/><input type="hidden" name="new_status" value="STATUS_REJECTED"/><input type="hidden"  name="' + soy.$$escapeHtml(opt_ijData.csrfName) + '" value="' + soy.$$escapeHtml(opt_ijData.csrfValue) + '"/><button type="submit" class="btn btn-warning" onclick="changeStatus(event, \'refuse\', ' + soy.$$escapeHtml(opt_data.id) + ');">\u041E\u0442\u043A\u043B\u043E\u043D\u0438\u0442\u044C</button></form></div>';
      break;
    case 'STATUS_COMMITED':
      output += '<form method="POST" name="refuse' + soy.$$escapeHtml(opt_data.id) + '"><input type="hidden" name="action" value="update_order_status"/><input type="hidden" name="order_id" value="' + soy.$$escapeHtml(opt_data.id) + '"/><input type="hidden" name="new_status" value="STATUS_REJECTED"/><input type="hidden"  name="' + soy.$$escapeHtml(opt_ijData.csrfName) + '" value="' + soy.$$escapeHtml(opt_ijData.csrfValue) + '"/><button type="submit" class="btn btn-xs btn-warning" onclick="changeStatus(event, \'refuse\', ' + soy.$$escapeHtml(opt_data.id) + ');">\u041E\u0442\u043A\u0430\u0437\u0430\u0442\u044C\u0441\u044F</button></form>';
      break;
    case 'STATUS_COMMITTED':
      output += '\u041E\u0436\u0434\u0438\u0430\u0435\u0442\u0441\u044F \u043F\u043E\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043D\u0438\u0435 \u043E\u0442\u0433\u0440\u0443\u0437\u043A\u0438';
      break;
    case 'STATUS_SHIPPED':
      output += '\u041E\u0436\u0434\u0438\u0430\u0435\u0442\u0441\u044F \u043F\u043E\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043D\u0438\u0435 \u0434\u043E\u0441\u0442\u0430\u0432\u043A\u0438';
      break;
    default:
      output += '&mdash;';
  }
  return output;
};
if (goog.DEBUG) {
  driver.order.buttons.soyTemplateName = 'driver.order.buttons';
}
