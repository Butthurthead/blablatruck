var mainMap, mapController, geoObjectParser;
var initCenterPoint = [55.76, 37.64];
var initZoom = 5;
var inputNames = ['wpFrom', 'wpTo'], currentPoint = null;

function viewTrip(initPoints) {
    var locationNames = [];
    $('#full_route li').each(function(index) {
        locationNames.push($(this).text());
    });
    $('#map').css({'opacity' : '0'});
    mainMap = new ymaps.Map("map", {
        center: initCenterPoint,
        zoom: initZoom
    });
    mapController = new MapController(mainMap);
    geoObjectParser = new GeoObjectParser();

    geoObjectParser.parseRoute(locationNames,
        function(route) {
            mapController.add(route);
            $('#map').css({'opacity' : '1'});
            addInitialPoints(initPoints);
        }, function(error) {
            mapController.addEmpty();
            console.log('Error: ' + error.message);
            alert('Невозможно построить маршрут');
        }
    );

    mapController.addClickHandler(function(event) {
        if (currentPoint == null)
            return;
        var cursorPos = [
            event.get('pageX'),
            event.get('pageY')
        ];
        var geoPos = mapController.cursorPosToGeoPos(cursorPos);
        var newObject = geoObjectParser.fromPoint(geoPos, {name: geoPos});
        mapController.replace(inputNames.indexOf(currentPoint) + 1, newObject);
        $('input[name="' + currentPoint + '"]').val(geoPos);
        currentPoint = null;
    });
}

function addInitialPoints(initPoints) {
    initPoints.forEach(function(item, i, arr) {
        var mapObject = geoObjectParser.fromGeoPosString(item, {name: item});
        if (mapObject != null) {
            mapController.add(mapObject);
        } else {
            geoObjectParser.fromAddress(item,
                function(foundObject) {
                    mapController.add(foundObject);
                },
                function(error) {
                    console.log('Error: ' + error.message);
                    alert('Объект не найден: ' + item);
                }
            );
        }
    });
}