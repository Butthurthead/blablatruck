function addMore(){
    var departureCity;
    var arrivalCity;
    var totalMass;
    var totalV;

    var param = location.
    search.
    slice(location.search.indexOf('?')+1);

    param = param + '';
    param = param.split('&');

    var result = [];

    for(var i = 0; i < param.length; i++) {
        var res = param[i].split('=');
        result[res[0]] = res[1];
    }

    if(result['departureCity']) {departureCity = result['departureCity']}
    else {departureCity = ""}
    if(result['arrivalCity']) {arrivalCity = result['arrivalCity']}
    else {arrivalCity = ""}
    if(result['totalMass']) {totalMass = result['totalMass']}
    else {totalMass = 0}
    if(result['totalV']) {totalV = result['totalV']}
    else {totalV = 0}

    var block =  true;

    $.ajax({
        url:"/findtrip",
        type:"GET",
        data:{lastTripId: last,
                departureCity: departureCity,
                arrivalCity: arrivalCity,
                totalMass: totalMass,
                totalV: totalV,
                _csrf: csrf},
        success:function(html) {
            if (html) {
                var trips = html.split('lastTripId')[0].split('<AND>');
                last = html.split('lastTripId')[1];
                if (last > 0) {
                    $('#mButton').fadeIn();
                    for (var i = 0; i < trips.length; i++) {
                        var trip = {};
                        trip.tripId = trips[i].split('->')[0];
                        trip.departure = trips[i].split('->')[1];
                        trip.departureCity = trips[i].split('->')[2];
                        trip.arrivalCity = trips[i].split('->')[3];
                        trip.description = trips[i].split('->')[4];
                        trip.status = trips[i].split('->')[5];
                        trip.arrival = trips[i].split('->')[6];
                        trip.totalMass = trips[i].split('->')[7];
                        trip.dimX = trips[i].split('->')[8];
                        trip.dimY = trips[i].split('->')[9];
                        trip.dimZ = trips[i].split('->')[10];
                        trip.pricePerKG = trips[i].split('->')[11];
                        trip.pricePerM2 = trips[i].split('->')[12];
                        var htmlResuilt = findTrips.tripNode({trip: trip});
                        $(htmlResuilt).appendTo($("#trips")).hide().fadeIn(1000);
                    }
                } else {
                    $('#mButton').hide();
                }
            }
            block = false;
        }
    });
}