var locations = [];
var idEl;

function doCompletion(id) {
    idEl = id;
    $.ajax({
        url : '/findtrip',
        type : 'GET',
        data : {action: 'complete',
                id: encodeURIComponent(document.getElementById(id).value)},
        success : function(doc) {
            if (doc != 'E'){
                locations = [];
                for (var i = 0; i < doc.split(',').length; i++){
                    locations[i] = doc.split(',')[i];
                }
                $("#"+idEl).autocomplete({
                    source: locations
                });
            }
        }
    });
}