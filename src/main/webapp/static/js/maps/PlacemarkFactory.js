function PlacemarkFactory() {

    function defaultPlacemark(point, name) {
        return new ymaps.Placemark(
            point,
            {hintContent: name},
            {preset: "islands#yellowStretchyIcon"}
        );
    }

    this.fromGeoObject = function(obj) {
        return defaultPlacemark(obj.geometry.getBounds()[0], obj.properties.get('name',''));
    };

    this.fromPoint = function(point, info) {
        return defaultPlacemark(point, info.name);
    }

}