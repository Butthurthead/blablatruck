function GeoObjectParser() {
    var placemarkFactory = new PlacemarkFactory();

    this.fromPoint = function(point, info) {
        return placemarkFactory.fromPoint(point, info);
    };

    this.fromAddress = function(address, onSuccess, onFail) {
        ymaps.geocode(address).then(
            function(res) {
                onSuccess(placemarkFactory.fromGeoObject(res.geoObjects.get(0)));
            },
            function(error) {
                onFail(error);
            }
        );
    };

    this.fromGeoPosString = function(geoPosString, info) {
        var splitted = geoPosString.split(',');
        var point = [0, 0];
        for (var i=0; i<2; i++) {
            var num = parseFloat(splitted[i]);
            if (isNaN(num))
                return null;
            point[i] = num;
        }
        return placemarkFactory.fromPoint(point, info);
    };

    this.parseRoute = function(locationNames, onSuccess, onFail) {
        var routePoints = [];
        locationNames.forEach(function(item, i, arr) {
            routePoints.push({point: item, type: 'wayPoint'});
        });
        ymaps.route(
            routePoints,
            {mapStateAutoApply: true}
        ).then(
            function (route) {
                onSuccess(route);
            },
            function(error) {
                onFail(error);
            }
        );
    }

}