function MapController(m) {
    var map = m;
    var objects = [];

    this.get = function(index) {
        return objects[index];
    };

    this.add = function(obj) {
        objects.push(obj);
        map.geoObjects.add(obj);
        return objects.indexOf(obj);
    };

    this.addEmpty = function() {
        objects.push(null);
    };

    this.remove = function(index) {
        var objToRemove = objects[index];
        objects.splice(index, 1);
        map.geoObjects.remove(objToRemove);
        return objToRemove;
    };

    this.replace = function(index, obj) {
        var removedObj = this.remove(index);
        objects.splice(index, 0, obj);
        map.geoObjects.add(obj);
        return removedObj;
    };

    this.cursorPosToGeoPos = function(cursorPos) {
        var projection = map.options.get('projection');
        return projection.fromGlobalPixels(
            map.converter.pageToGlobal(cursorPos),
            map.getZoom()
        );
    };

    this.addClickHandler = function(handler) {
        map.events.add('click', function(event) {
            handler(event.get('domEvent'));
        });
    }

}