var routePoints = [null, null];
var prevRoute = null;
var viaPointsCount = 0;
var via = [];

function createTrip() {

    mainMap = new ymaps.Map("map", {
        center: initCenterPoint,
        zoom: initZoom
    });

    $('#departureCity').change(function() {
        routePoints[0] = {point: $(this).val(), type: 'wayPoint'};
        updateRoute();
    });

    $('#arrivalCity').change(function() {
        routePoints[1] = {point: $(this).val(), type: 'wayPoint'};
        updateRoute();
    });
}

function updateRoute() {
    if ((routePoints[0] == null)||(routePoints[1] == null)) {
        return;
    }
    var size = 0;
    for(var i = 1; i <= viaPointsCount; i++){
        if ($('#via'+i).val() != null && $('#via'+i).val() != ''){
            via[size] = $('#via'+i).val();
            size++;
        }
    }
    var route = [];
    if (size > 0){
        sendData();
        route[0] = routePoints[0];
        for(var i = 1; i <= size; i++){
            route[i] = via[i-1];
        }
        route[size+1] = routePoints[1];
    } else {
        route = routePoints;
    }
    ymaps.route(
        route,
        {mapStateAutoApply: true}
    ).then(function(route) {
        if (prevRoute != null) {
            mainMap.geoObjects.remove(prevRoute);
        }
        mainMap.geoObjects.add(route);
        prevRoute = route;
    }, function(error) {
        console.log('Error: ' + error.message);
        alert('Не удалось построить маршрут');
    });
}

function addpoint(){
    viaPointsCount++;
    var doc = '<div id="f-g'+viaPointsCount+'" class="form-group">' +
                '<label class="col-sm-4 control-label">Остановка</label>' +
                '<div class="col-sm-8" style="width: 60% !important;">'+
                    '<input class="form-control" type="text" id="via'+viaPointsCount+'" value="" placeholder="Промежуточная точка"/>' +
                '</div>' +
                '<div style="width: 6%; float: left">' +
                    '<input type="button" class="button-rempoint" onclick="rempoint('+viaPointsCount+')" value="X"/>' +
                '</div>' +
            '</div>';
    $(doc).appendTo($('#points'));
    $('#via'+viaPointsCount).change(function(){
       updateRoute();
    });
}

function rempoint(num){
    $('#via'+num).val('');
    $('#f-g'+num).hide();
    updateRoute();
}

function sendData(){
    $('#hidden').html('<input type="hidden" name="viaPoint" value="'+via+'"/>');
}