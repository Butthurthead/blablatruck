function toolbarButtons() {
    $('.btn-toolbar button').click(function() {
        var id = $(this).attr('id');
        var visible = '';
        switch (id) {
            case 'all':
                visible = 'tr.order';
                break;
            case 'new':
                visible = 'tr.order.STATUS_NEW';
                break;
            case 'actual':
                visible = 'tr.order.STATUS_COMMITTED, tr.order.STATUS_SHIPPED';
                break;
            case 'delivered':
                visible = 'tr.order.STATUS_DELIVERED';
                break;
            case 'rejected':
                visible = 'tr.order.STATUS_REJECTED';
                break;
            default:
                return;
        }
        $('tr.order').hide();
        $(visible).show();
    });
}