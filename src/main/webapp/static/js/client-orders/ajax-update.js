function ajaxOnButtons() {
    $(document).on('click', 'form button', function(event) {
        event.preventDefault();
        var form = $(this).closest('form');
        var formData = form.serializeArray();
        var update = ($(this).attr('name') == 'status');
        if (update) {
            formData.push({name : $(this).attr('name'), value : $(this).attr('value')});
            var newStatus = $(this).attr('value');
            $(this).closest('tr').attr('class', 'order ' + newStatus);
        }
        $.ajax({
            url : form.attr('action'),
            type : 'POST',
            data : formData,
            success : function(result) {
                if (update) {
                    form.closest('td').replaceWith('<td>' + result + '</td>');
                    updateButtonsStyle();
                } else {
                    form.closest('tr').remove();
                }
            },
            error : function(result) {
                alert('Произошла ошибка');
            }
        });
    });
}