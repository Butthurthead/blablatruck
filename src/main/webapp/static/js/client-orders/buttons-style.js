function updateButtonsStyle() {
    var buttons = document.getElementsByTagName('button');
    for (var i=0; i<buttons.length; i++) {
        var value = buttons[i].getAttribute('value');
        if (value == 'STATUS_COMMITTED' || value == 'STATUS_SHIPPED') {
            buttons[i].className += ' btn btn-info';
        } else if (value == "STATUS_REJECTED") {
            buttons[i].className += ' btn btn-warning';
        } else if (value == "STATUS_DELIVERED") {
            buttons[i].className += ' btn btn-success';
        }
    }
}